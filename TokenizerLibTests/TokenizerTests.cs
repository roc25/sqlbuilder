using System.Linq;
using TokenizerLib;
using Xunit;

namespace TokenizerLibTests
{
    public class TokenizerTests
    {
        [Theory]
        [InlineData("val1 && val2", "0,4,1:val1", "1,2,3:&&", "1,4,1:val2")]
        [InlineData(" A    function( \"Hello\" , 5, 3.5 )", "1,1,1:A", "4,8,1:function", "0,1,2:(", "1,7,6:Hello", "1,1,2:,", "1,1,4:5", "0,1,2:,", "1,3,5:3.5", "1,1,2:)")]
        [InlineData(" @\"This is a\n\"\"verbatim\"\" string\" ", "1,32,6:This is a\n\"verbatim\" string")]
        [InlineData("=@\"This is a\n\"\"verbatim\"\" string\" ", "0,1,3:=", "0,32,6:This is a\n\"verbatim\" string")]
        [InlineData(" $\"Interpolated { object.value }} string with single {{}, double {{{{}}}} and normal {{braces}}\" ;", "1,16,6:Interpolated ", "1,6,1:object", "0,1,3:.", "0,5,1:value", "1,65,6:} string with single {}, double {{}} and normal {braces}", "1,1,2:;")]
        [InlineData("2.5>$\"{(a ? b : c)}\"", "0,3,5:2.5", "0,1,3:>", "0,3,6:", "0,1,2:(", "0,1,1:a", "1,1,3:?", "1,1,1:b", "1,1,3::", "1,1,1:c", "0,1,2:)", "0,2,6:")]
        [InlineData("2>$@\"\"\"{(a ? b : c):Format}\"\"\"", "0,1,4:2", "0,1,3:>", "0,6,6:\"", "0,1,2:(", "0,1,1:a", "1,1,3:?", "1,1,1:b", "1,1,3::", "1,1,1:c", "0,1,2:)", "0,11,6:Format}\"")]
        [InlineData("@$\"{({a}){b}}\"\"\".t", "0,4,6:", "0,1,2:(", "0,1,2:{", "0,1,1:a", "0,1,2:}", "0,1,2:)", "0,1,2:{", "0,1,1:b", "0,1,2:}", "0,4,6:\"", "0,1,3:.", "0,1,1:t")]
        public void Parse_CSharpStyle_Success(string expression, params string[] expectedTokens) // expectedToken: SignificantPartStartPos,SignificantPartLength,TokenType:Value
        {
            var tokenStream = Tokenizer.Parse(expression, Tokenizer.Mode.CSharp);

            Assert.Equal(expectedTokens.Length, tokenStream.TokenList.Length);
            for (var i = 0; i < expectedTokens.Length; ++i)
            {
                var valPos = expectedTokens[i].IndexOf(':');
                var attributes = expectedTokens[i].Substring(0, valPos).Split(',').Select(int.Parse).ToArray();
                var startPos = attributes[0];
                var significantLength = attributes[1];
                var tokenType = (TokenType)attributes[2];
                var tokenValue = expectedTokens[i].Substring(valPos + 1);

                Assert.Equal(tokenType, tokenStream.TokenList[i].Type);
                Assert.Equal(startPos, (int)tokenStream.TokenList[i].SignificantPart.Start);
                Assert.Equal(significantLength, (int)tokenStream.TokenList[i].SignificantPart.Length);
                Assert.Equal(tokenValue, tokenStream.TokenList[i].Value.ToString());
            }
        }

        [Theory]
        [InlineData("SELECT [Table1].[Col[umn]](1] FROM [Table1]", "0,6,1:SELECT", "1,8,8:Table1", "0,1,3:.", "0,13,8:Col[umn](1", "1,4,1:FROM", "1,8,8:Table1")]
        public void Parse_SqlStyle_Success(string expression, params string[] expectedTokens) // expectedToken: SignificantPartStartPos,SignificantPartLength,TokenType:Value
        {
            var tokenStream = Tokenizer.Parse(expression, Tokenizer.Mode.SQL);

            Assert.Equal(expectedTokens.Length, tokenStream.TokenList.Length);
            for (var i = 0; i < expectedTokens.Length; ++i)
            {
                var valPos = expectedTokens[i].IndexOf(':');
                var attributes = expectedTokens[i].Substring(0, valPos).Split(',').Select(int.Parse).ToArray();
                var startPos = attributes[0];
                var significantLength = attributes[1];
                var tokenType = (TokenType)attributes[2];
                var tokenValue = expectedTokens[i].Substring(valPos + 1);

                Assert.Equal(tokenType, tokenStream.TokenList[i].Type);
                Assert.Equal(startPos, (int)tokenStream.TokenList[i].SignificantPart.Start);
                Assert.Equal(significantLength, (int)tokenStream.TokenList[i].SignificantPart.Length);
                Assert.Equal(tokenValue, tokenStream.TokenList[i].Value.ToString());
            }
        }
    }
}
