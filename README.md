# SqlBuilder

SqlBuilder is a C# framework to create and execute database commands without using string literals. The C# compiler can verify your statements for correct SQL expressions and you can use Intellisense to build your statements.

Example to read one value from a database table:
~~~~
var userName = database.SelectFrom(Tables.User).Where(Tables.User.Id == 5).ReadValue(Tables.User.Name);
~~~~

Its core implementation doesn't use reflection or interprets LINQ expression trees to build SQL statements. Its meant to be easy with smart code, keeping the full potential of SQL. The downside is that conditional expressions can't use double '||' and '&&' for SQL 'OR' and 'AND'. Instead single '|' and '&' operators are used.

A principle is, that your code can run on any database. Currently SqlBuilder can create statements for ORACLE, SQL Server and SQLite databases. The code is designed to be extensible, so other databases can be added without affecting the your implementations.

SqlBuilder has a database first approach. Entity Framework for example suggests a code-first approach. Database first has the advantage that you can design your database according to your needs (performance, maintainability, etc.) and the code reflects your database schema.

To achieve this a code generator is included in the solution that creates the C# table classes from the SQL Create Table scripts of your database schema. Currently only SQL Server scripts are supported.

For more details see SqlBuilderFramework/Readme.md or the SqlBuilderSamplesAndTests.
