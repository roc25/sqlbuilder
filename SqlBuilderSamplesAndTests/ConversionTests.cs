﻿using SqlBuilderFramework;
using System;
using Xunit;

namespace SqlBuilderSamplesAndTests
{
    public class ConversionTests
    {
        private enum Test { ev1, ev2 }

        [Fact]
        public void Convert_NoCondition_Converted()
        {
            Assert.True((bool)Convert.ChangeType(1, TypeCode.Boolean));
            Assert.True((bool)Convert.ChangeType(-1, TypeCode.Boolean));
            Assert.False((bool)Convert.ChangeType(0, TypeCode.Boolean));
            Assert.True((bool)Convert.ChangeType((int?)1, TypeCode.Boolean));
            Assert.False((bool)Convert.ChangeType((int?)0, TypeCode.Boolean));

            Assert.True((bool)DbValueConverter.ChangeType(1, typeof(bool)));
            Assert.True((bool)DbValueConverter.ChangeType(-1, typeof(bool)));
            Assert.False((bool)DbValueConverter.ChangeType(0, typeof(bool)));
            Assert.True((bool)DbValueConverter.ChangeType((int?)1, typeof(bool)));
            Assert.False((bool)DbValueConverter.ChangeType((int?)0, typeof(bool)));
        }
    }
}
