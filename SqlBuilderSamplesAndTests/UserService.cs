using KendoNET.DynamicLinq;
using Microsoft.IdentityModel.Tokens;
using SqlBuilderFramework;
using SqlBuilderSamplesAndTests.DbTables;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace SqlBuilderSamplesAndTests
{
    public class UserService
    {
        private const string _secret = "ANd HeRe we go. 4 ThIS pROjEct we neEd a gOoD SeCrET ConfiG!!"; // From AppSettings

        private readonly IDatabase _database;

        public UserService(IDatabase database)
        {
            _database = database;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model, string ipAddress)
        {
            var tblRefreshToken = new DbTables.RefreshToken();

            var query = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.IsActive == true & Tables.Users.Columns.Username == model.Username);

            var user = query.ReadAll(User.Mapper).SingleOrDefault();

            if (user == null)
                return null;

            if (!CryptVerify(model.Password, user.Password))
                return null;

            // authentication successful so generate jwt and refresh tokens
            var jwtToken = generateJwtToken(user);
            var refreshToken = generateRefreshToken(ipAddress);

            // save refresh token
            _database.Insert
                .In(tblRefreshToken)
                .Set(tblRefreshToken.Columns.Token.To(refreshToken.Token),
                     tblRefreshToken.Columns.UserId.To(user.Id),
                     tblRefreshToken.Columns.Expires.To(refreshToken.Expires),
                     tblRefreshToken.Columns.Created.To(refreshToken.Created),
                     tblRefreshToken.Columns.CreatedByIp.To(refreshToken.CreatedByIp))
                .Execute();

            return new AuthenticateResponse(user, jwtToken, refreshToken.Token);
        }

        public AuthenticateResponse RefreshToken(string token, string ipAddress)
        {
            var refreshToken = new RefreshToken();
            var user = new User();

            var tblRefreshToken = new DbTables.RefreshToken();
            var tblUsers = new Users();

            var query = _database.Select
                .From(tblRefreshToken.InnerJoin(tblUsers, tblRefreshToken.Columns.UserId == tblUsers.Columns.Id))
                .Where(tblRefreshToken.Columns.Token == token);

            query.Map(tblRefreshToken.Columns.Token, value => refreshToken.Token = value);
            query.Map(tblRefreshToken.Columns.Expires, value => refreshToken.Expires = value);
            query.Map(tblRefreshToken.Columns.Revoked, value => refreshToken.Revoked = value);

            query.Map(tblRefreshToken.Columns.UserId, value => user.Id = value);
            query.Map(tblUsers.Columns.FirstName, value => user.FirstName = value);
            query.Map(tblUsers.Columns.LastName, value => user.LastName = value);
            query.Map(tblUsers.Columns.Username, value => user.Username = value);
            query.Map(tblUsers.Columns.Role, value => user.Role = value);

            query.ReadFirst();

            if (user.Id == 0 || !refreshToken.IsActive) // return null if token is no longer active
                return null;

            // replace old refresh token with a new one and save
            var newRefreshToken = generateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;

            // update old Refresh Token
            _database.Update.In(tblRefreshToken).Where(tblRefreshToken.Columns.Token == token)
                .Set(tblRefreshToken.Columns.ReplacedByToken.To(refreshToken.ReplacedByToken),
                    tblRefreshToken.Columns.Revoked.To(refreshToken.Revoked),
                    tblRefreshToken.Columns.RevokedByIp.To(refreshToken.RevokedByIp))
                .Execute();

            // insert new Refresh Token
            _database.Insert
                .In(tblRefreshToken)
                .Set(tblRefreshToken.Columns.Token.To(newRefreshToken.Token),
                    tblRefreshToken.Columns.UserId.To(user.Id),
                    tblRefreshToken.Columns.Expires.To(newRefreshToken.Expires),
                    tblRefreshToken.Columns.Created.To(newRefreshToken.Created),
                    tblRefreshToken.Columns.CreatedByIp.To(newRefreshToken.CreatedByIp))
                .Execute();

            // generate new jwt
            var jwtToken = generateJwtToken(user);

            return new AuthenticateResponse(user, jwtToken, newRefreshToken.Token);
        }

        public bool RevokeToken(string token, string ipAddress)
        {
            var tblRefreshToken = new DbTables.RefreshToken();

            var queryToken = _database.Select
                .From(tblRefreshToken)
                .Where(tblRefreshToken.Columns.Token == token);

            var refreshToken = queryToken.ReadAll(new DbMapper<RefreshToken>()
                .Map(tblRefreshToken.Columns.Token, (rt, value) => rt.Token = value)
                .Map(tblRefreshToken.Columns.Expires, (rt, value) => rt.Expires = value)
                .Map(tblRefreshToken.Columns.Revoked, (rt, value) => rt.Revoked = value)
                ).SingleOrDefault();

            if (refreshToken == null || !refreshToken.IsActive) // return false if token is not active
                return false;

            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;

            _database.Update
                .In(tblRefreshToken)
                .Where(tblRefreshToken.Columns.Token == token)
                .Set(tblRefreshToken.Columns.Revoked.To(refreshToken.Revoked),
                     tblRefreshToken.Columns.RevokedByIp.To(refreshToken.RevokedByIp))
                .Execute();

            return true;
        }

        public List<User> GetAll()
        {
            var query = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.IsActive == true)
                .OrderBy(Tables.Users.Columns.Id);

            return query.ReadAll(User.Mapper);
        }

        public DataSourceResult GetByRequest(DataSourceRequest request)
        {
            if (request == null)
                return null;

            var query = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.IsActive == true);

            var columnMap = new Dictionary<string, DbColumn>(StringComparer.OrdinalIgnoreCase)
            {
                [nameof(User.Id)] = Tables.Users.Columns.Id,
                [nameof(User.FirstName)] = Tables.Users.Columns.FirstName,
                [nameof(User.LastName)] = Tables.Users.Columns.LastName,
                [nameof(User.Username)] = Tables.Users.Columns.Username,
                [nameof(User.Role)] = Tables.Users.Columns.Role,
            };

            DataSourceRequestExtensions.ApplyToQuery(query, columnMap, request);

            if (!query.IsOrdered)
                query.OrderBy(Tables.Users.Columns.Id);

            return new DataSourceResult
            {
                Data = query.ReadAll(User.Mapper),
                Total = _database.Select.From(Tables.Users).Where(query.Constraint).ReadValue(Tables.Users.Columns.Id.Count())
            };
        }

        public User GetById(int id)
        {
            var query = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.Id == id);

            return query.ReadAll(User.Mapper).SingleOrDefault();
        }

        public void Add(UserNew user)
        {
            var existingUserId = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.Username == user.Username)
                .ReadValue(Tables.Users.Columns.Id);

            if (existingUserId != 0)
                throw new ApplicationException("Dieser Benutzername wurde bereits vergeben");

            // generate a password
            var password = GeneratePassword();
            var pwHash = CryptHash(password);

            // insert
            _database.Insert
                .In(Tables.Users)
                .Set(Tables.Users.Columns.Username.To(user.Username),
                     Tables.Users.Columns.FirstName.To(user.FirstName),
                     Tables.Users.Columns.LastName.To(user.LastName),
                     Tables.Users.Columns.Role.To(user.Role),
                     Tables.Users.Columns.IsActive.To(true),
                     Tables.Users.Columns.Password.To(pwHash))
                .Execute();

            // TODO: Send email
        }

        public void Delete(int id)
        {
            var otherAdminId = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.Id != id & Tables.Users.Columns.IsActive == true & Tables.Users.Columns.Role == "Admin")
                .ReadValue(Tables.Users.Columns.Id);

            if (otherAdminId == 0)
                throw new ApplicationException("Sie k�nnen den letzen Benutzer mit der Rolle Admin nicht l�schen");

            var deletedUser = _database.Update
                .In(Tables.Users)
                .Where(Tables.Users.Columns.Id == id)
                .Set(Tables.Users.Columns.IsActive.To(false))
                .Execute();

            if (deletedUser == 0)
            {
                throw new ApplicationException("Der Benutzer wurde durch einen Benutzer gel�scht");
            }
        }

        public void Update(UserChanges userChanges)
        {
            if (userChanges.Username != userChanges.OrigUsername)
            {
                var otherUser = _database.Select
                    .From(Tables.Users)
                    .Where(Tables.Users.Columns.Username == userChanges.Username & Tables.Users.Columns.IsActive == true)
                    .ReadValue(Tables.Users.Columns.Id);

                if (otherUser != 0)
                    throw new ApplicationException("Dieser Benutzername wurde bereits vergeben");
            }

            if (userChanges.Role != "Admin" && userChanges.OrigRole == "Admin")
            {
                var otherAdmin = _database.Select
                    .From(Tables.Users)
                    .Where(Tables.Users.Columns.Id != userChanges.Id & Tables.Users.Columns.IsActive == true & Tables.Users.Columns.Role == "Admin")
                    .ReadValue(Tables.Users.Columns.Id);

                if (otherAdmin == 0)
                    throw new ApplicationException("Mindestens ein Benutzer ben�tigt die Rolle Admin. Eine �nderung ist nicht m�glich");
            }

            var updatedUser = _database.Update
                .In(Tables.Users)
                .Where(Tables.Users.Columns.Id == userChanges.Id)
                .Set(Tables.Users.Columns.Username.To(userChanges.Username),
                    Tables.Users.Columns.FirstName.To(userChanges.FirstName),
                    Tables.Users.Columns.LastName.To(userChanges.LastName),
                    Tables.Users.Columns.Role.To(userChanges.Role))
                .Execute();

            if (updatedUser == 0)
            {
                throw new ApplicationException("Der Benutzer wurde durch einen anderen Benutzer gel�scht");
            }
        }

        public void ChangePassword(ChangeUserPassword user)
        {
            var password = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.Id == user.Id)
                .ReadValue(Tables.Users.Columns.Password);

            if (password == null)
                throw new ApplicationException("Der angemeldete Benutzer konnte nicht gefunden werden");

            // check password/hash
            if (!CryptVerify(user.Password, password))
                throw new ApplicationException("Das eingegebenen Kennwort ist ung�ltig");

            _database.Update.In(Tables.Users).Where(Tables.Users.Columns.Id == user.Id)
                .Set(Tables.Users.Columns.IsActive.To(true))
                .Set(Tables.Users.Columns.Password.To(CryptHash(user.NewPassword)))
                .Execute();
        }

        public void ResetPassword(ResetPasswordRequest request)
        {
            if (request == null)
                return;

            var query = _database.Select
                .From(Tables.Users)
                .Where(Tables.Users.Columns.Username == request.Username & Tables.Users.Columns.IsActive == true);

            var user = query.ReadAll(User.Mapper).SingleOrDefault();

            if (user == null)
                throw new ApplicationException("Der eingegebene Benutzername existiert nicht");

            // generate a new password
            var password = GeneratePassword();
            var pwHash =CryptHash(password);

            _database.Update.In(Tables.Users).Where(Tables.Users.Columns.Id == user.Id)
                .Set(Tables.Users.Columns.Password.To(pwHash))
                .Execute();

            // TODO: Send email
        }

        // helper methods
        private string generateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddMinutes(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private RefreshToken generateRefreshToken(string ipAddress)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.UtcNow.AddDays(7),
                    Created = DateTime.UtcNow,
                    CreatedByIp = ipAddress
                };
            }
        }

        private string GeneratePassword()
        {
            return "ThisIsARandom!Password";
        }

        private string CryptHash(string password)
        {
            return string.Empty;
        }

        private bool CryptVerify(string password1, string password2)
        {
            return true;
        }
    }
}