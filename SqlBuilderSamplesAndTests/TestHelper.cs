﻿using Microsoft.Extensions.Options;

namespace SqlBuilderSamplesAndTests
{
    public class Options<T> : IOptions<T> where T: class, new()
    {
        public T Value { get; set; }
    }

    public static class TestHelper
    {
        private static DatabaseManager DatabaseManager;

        public static DatabaseManager GetTestDatabaseManager()
        {
            if (DatabaseManager == null)
            {
                var databaseOptions = new Options<DatabaseSettings>();
                databaseOptions.Value = new DatabaseSettings
                {
                    Provider = "SQLite",
                    Database = ":memory:"
                };

                DatabaseManager = new DatabaseManager(databaseOptions);
            }

            return DatabaseManager;
        }
    }
}
