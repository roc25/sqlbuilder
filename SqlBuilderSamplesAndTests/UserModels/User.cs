using SqlBuilderFramework;
using SqlBuilderSamplesAndTests.DbTables;
using System.Text.Json.Serialization;

namespace SqlBuilderSamplesAndTests
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }

        [JsonIgnore]
        public string Password { get; set; }

        public static DbMapper<User> Mapper =>
            new DbMapper<User>()
                .Map(Tables.Users.Columns.Id, (user, value) => user.Id = value)
                .Map(Tables.Users.Columns.FirstName, (user, value) => user.FirstName = value)
                .Map(Tables.Users.Columns.LastName, (user, value) => user.LastName = value)
                .Map(Tables.Users.Columns.Username, (user, value) => user.Username = value)
                .Map(Tables.Users.Columns.Role, (user, value) => user.Role = value)
                .Map(Tables.Users.Columns.Password, (user, value) => user.Password = value);
    }

    public class ChangeUserPassword
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }

    public class UserNew
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName  { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
    }
}