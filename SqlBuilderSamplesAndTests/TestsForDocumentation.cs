﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlBuilderFramework;
using SqlBuilderSamplesAndTests.DbTables;
using Xunit;

namespace SqlBuilderSamplesAndTests
{
    public class TestsForDocumentation
    {
        private readonly IDatabase _database;

        public TestsForDocumentation()
        {
            var databaseOptions = new Options<DatabaseSettings>
            {
                Value = new DatabaseSettings
                {
                    Provider = "SQLite",
                    Database = ":memory:"
                }
            };

            var dbManager = new DatabaseManager(databaseOptions);

            _database = dbManager.Connect();
        }

        [Fact]
        public void Query_SingleResultViaBindings_Success()
        {
            var tblUsers = new Users();

            var sql = _database.Select.From(tblUsers).Where(tblUsers.Columns.Id == 1);

            var firstName = sql.Bind(tblUsers.Columns.FirstName);
            var lastName = sql.Bind(tblUsers.Columns.LastName);

            sql.ReadFirst();

            Assert.Equal("Armin", firstName.Value);
            Assert.Equal("Administrator", lastName.Value);
        }

        [Fact]
        public void Query_AllResultsViaBindings_Success()
        {
            var tblUsers = new Users();

            var sql = _database.Select.From(tblUsers);

            var firstName = sql.Bind(tblUsers.Columns.FirstName);
            var lastName = sql.Bind(tblUsers.Columns.LastName);

            var recordsRead = 0;

            using (var reader = sql.ExecuteReader())
            {
                while (reader.Next())
                {
                    recordsRead++;
                    Assert.False(string.IsNullOrEmpty(firstName.Value));
                    Assert.False(string.IsNullOrEmpty(lastName.Value));
                }
            }

            Assert.Equal(3, recordsRead);
        }

        [Fact]
        public void Query_ExplicitClassMappings_Success()
        {
            var users = _database.SelectFrom(Tables.Users).ReadAll(BoUser.MapFrom(Tables.Users));

            Assert.Equal(3, users.Count);
        }

        [Fact]
        public void Query_ImplicitClassMappings_Success()
        {
            var users = _database.SelectFrom(Tables.Users).ReadInto<BoUser>();

            Assert.Equal(3, users.Count);
        }

        [Fact]
        public void Query_ImplicitMappingOf2Classes_Success()
        {
            var rt = new DbTables.RefreshToken();

            _database.InsertInto(rt).Set(rt.Columns.Token.To("Token"), rt.Columns.UserId.To(2), rt.Columns.Expires.To(new DateTime(21, 1, 2)), rt.Columns.Created.To(DateTime.Now), rt.Columns.CreatedByIp.To("IP")).ExecuteNonQuery();

            var result = _database.SelectFrom(Tables.RefreshToken.InnerJoin(Tables.Users)).ReadInto<BoRefreshToken, BoUser>();

            Assert.Equal(2, result.Single().Item1.UserId);
        }

        [Fact]
        public void InlineView_DefinedColumns_Success()
        {
            // Arrange

            var tblUsers = new Users();

            var inlineTable = _database.Select.From(tblUsers).ToTable(new { Id = tblUsers.Columns.Id });

            var query = _database.Select.From(inlineTable).OrderBy(inlineTable.Columns.Id).Returning(inlineTable.Columns.Id);

            // Act

            var sql = query.Sql(null);
            var values = query.ReadValues(inlineTable.Columns.Id);

            // Assert

            Assert.Equal("SELECT T1.COL1 COL1 FROM (SELECT S1T1.\"Id\" COL1 FROM \"USERS\" AS S1T1) AS T1 ORDER BY COL1", sql);
            Assert.Equal(new [] { 1, 2, 3 }, values);
        }

        [Fact]
        public void InsertFrom_NoCondition_Success()
        {
            var tblUsers = new Users();

            var lastId = _database.SelectFrom(tblUsers).ReadValue(tblUsers.Columns.Id.Max());

            var command = _database.InsertInto(tblUsers).From(SqlBuilder.SelectFrom(tblUsers).Where(tblUsers.Columns.Id == 2))
                .Set(tblUsers.Columns.IsActive.To(tblUsers.Columns.IsActive))
                .Set(tblUsers.Columns.FirstName.To(tblUsers.Columns.FirstName))
                .Set(tblUsers.Columns.LastName.To(tblUsers.Columns.LastName))
                .Set(tblUsers.Columns.Username.To("NewUserName"))
                .Set(tblUsers.Columns.Password.To(tblUsers.Columns.Password))
                .Set(tblUsers.Columns.Role.To(tblUsers.Columns.Role))
                ;

            var sql = command.Sql(null);

            command.ExecuteNonQuery();

            Assert.Equal("INSERT INTO \"USERS\" (\"IsActive\", \"FirstName\", \"LastName\", \"Username\", \"Password\", \"Role\") SELECT S1T1.\"IsActive\" COL1, S1T1.\"FirstName\" COL2, S1T1.\"LastName\" COL3, 'NewUserName' COL4, S1T1.\"Password\" COL5, S1T1.\"Role\" COL6 FROM \"USERS\" AS S1T1 WHERE (S1T1.\"Id\" = 2)", sql);
            Assert.Equal(lastId + 1, _database.SelectFrom(tblUsers).ReadValue(tblUsers.Columns.Id.Max()));
        }

        [Fact]
        public void SetByObject_SampleDataObject_InsertSuccessful()
        {
            // Arrange

            var businessClass = new BoUser
            {
                FirstName = "Max",
                LastName = "Muster",
                Password = "Passwort",
                UserName = "MM",
                UserRole = "Role",
                IsActive = true
            };

            // Act

            _database.Insert.In(Tables.Users).Set(businessClass).Map(Tables.Users.Columns.Id, value => businessClass.Id = value).Execute();

            // Assert

            Assert.Equal(4, businessClass.Id);
        }

        [Fact]
        public void SelectByEnum_SampleCategory_CategorySelected()
        {
            // Arrange

            // Act
            var result = _database.Select.From(Tables.Categories).Where(Tables.Categories.Columns.CategoryId == BycicleCategory.Comfort).ReadValue(Tables.Categories.Columns.CategoryName);

            // Assert
            Assert.Equal("Comfort Bicycles", result);
        }

        private enum BycicleCategory
        {
            Children = 1,
            Comfort = 2,
            Cruisers = 3,
            Cyclocross = 4,
            Electric = 5,
            Mountain = 6,
            Road = 7,
        }

        private class BoUser : IDbInsertAssignments<Users>
        {
            public int Id { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string Password { get; set; }

            public string UserName { get; set; }

            public string UserRole { get; set; }

            public bool IsActive { get; set; }

            public static DbMapper<BoUser> MapFrom(Users tblUsers)
            {
                if (tblUsers == null)
                {
                    throw new ArgumentNullException(nameof(tblUsers));
                }

                return new DbMapper<BoUser>()
                    .Map(tblUsers.Columns.Id, (user, id) => user.Id = id)
                    .Map(tblUsers.Columns.FirstName, (user, firstName) => user.FirstName = firstName)
                    .Map(tblUsers.Columns.LastName, (user, lastName) => user.LastName = lastName)
                    .Map(tblUsers.Columns.Password, (user, password) => user.Password = password)
                    .Map(tblUsers.Columns.Username, (user, username) => user.UserName = username)
                    .Map(tblUsers.Columns.Role, (user, role) => user.UserRole = role)
                    .Map(tblUsers.Columns.IsActive, (user, isActive) => user.IsActive = isActive)
                    ;
            }

            public IEnumerable<DbAssignment> GetAssignments(Users table)
            {
                return new[]
                {
                    table.Columns.FirstName.To(FirstName),
                    table.Columns.LastName.To(LastName),
                    table.Columns.Password.To(Password),
                    table.Columns.Username.To(UserName),
                    table.Columns.Role.To(UserRole),
                    table.Columns.IsActive.To(IsActive)
                };
            }
        }

        private class BoRefreshToken
        {
            public int UserId { get; set; }

            public string Token { get; set; }

            public DateTime Expires { get; set; }

            public bool IsExpired => DateTime.UtcNow >= Expires;

            public DateTime Created { get; set; }

            public string CreatedByIp { get; set; }

            public DateTime? Revoked { get; set; }

            public string RevokedByIp { get; set; }

            public string ReplacedByToken { get; set; }

            public bool IsActive => Revoked == null && !IsExpired;

            public static DbMapper<BoRefreshToken> MapFrom(DbTables.RefreshToken tblRefreshToken)
            {
                return new DbMapper<BoRefreshToken>()
                        .Map(tblRefreshToken.Columns.UserId, (rt, value) => rt.UserId = value)
                        .Map(tblRefreshToken.Columns.Token, (rt, value) => rt.Token = value)
                        .Map(tblRefreshToken.Columns.Expires, (rt, value) => rt.Expires = value)
                        .Map(tblRefreshToken.Columns.Revoked, (rt, value) => rt.Revoked = value)
                    ;
            }
        }
    }
}
