using SqlBuilderFramework;
using SqlBuilderSamplesAndTests.DbTables;
using System;
using Xunit;

namespace SqlBuilderSamplesAndTests
{
    public class BasicTests
    {
        [Fact]
        public void DeleteUnusedEntities()
        {
            var database = TestHelper.GetTestDatabaseManager().Connect();

            var sql = database.Delete.From(Tables.Stocks).Where(Tables.Stocks.Columns.StoreId == 1).Sql(null);
            Assert.Equal(313, database.Delete.From(Tables.Stocks).Where(Tables.Stocks.Columns.StoreId == 1).ExecuteNonQuery());
            var deleter = database.Delete.From(Tables.Products).Where(Tables.Products.Columns.ProductId.NotIn(SqlBuilder.Select.From(Tables.Stocks).Returning(Tables.Stocks.Columns.ProductId)));
            Assert.Equal("DELETE FROM \"PRODUCTS\" WHERE (\"product_id\" NOT IN (SELECT S1T1.\"product_id\" COL1 FROM \"STOCKS\" AS S1T1))", deleter.Sql(null));
            Assert.Equal(8, deleter.ExecuteNonQuery());
        }

        [Fact]
        public void ConcatUserName()
        {
            var database = TestHelper.GetTestDatabaseManager().Connect();

            var tblUsers = new Users();

            var userNames = database.Select
                                    .From(tblUsers)
                .ReadValues(
                    DbColumn.CaseWhen<string>(
                        tblUsers.Columns.LastName.IsNull() | tblUsers.Columns.LastName == string.Empty,
                        tblUsers.Columns.FirstName,
                        tblUsers.Columns.FirstName.ConcatWith(" ").ConcatWith(tblUsers.Columns.LastName)));

            Assert.Equal(new[] { "Armin Administrator", "Max Mustermann", "Anton Tester" }, userNames);
        }

        [Fact]
        public void AddOrder()
        {
            var database = TestHelper.GetTestDatabaseManager().Connect();

            Assert.Equal(1616, database.Insert
                .In(Tables.Orders)
                .Set(
                    Tables.Orders.Columns.CustomerId.To(1),
                    Tables.Orders.Columns.OrderStatus.To(2),
                    Tables.Orders.Columns.OrderDate.To(new DateTime(2020, 6, 6)),
                    Tables.Orders.Columns.RequiredDate.To(new DateTime(2020, 9, 30)),
                    Tables.Orders.Columns.StoreId.To(1),
                    Tables.Orders.Columns.StaffId.To(1))
                .ReadValue(Tables.Orders.Columns.OrderId));
        }

        [Fact]
        public void Join_DefaultConstraint_Expected()
        {
            var database = TestHelper.GetTestDatabaseManager().Connect();

            var query = database.Select.From(Tables.Products.InnerJoin(Tables.Categories).LeftJoin(Tables.Brands)).Returning(Tables.Products.Columns.ProductName, Tables.Categories.Columns.CategoryName, Tables.Brands.Columns.BrandName);

            var sql = query.Sql(null);

            var expected = "SELECT T1.\"product_name\" COL1, T2.\"category_name\" COL2, T3.\"brand_name\" COL3 FROM \"PRODUCTS\" AS T1 INNER JOIN \"CATEGORIES\" AS T2 ON (T1.\"category_id\" = T2.\"category_id\") LEFT OUTER JOIN \"BRANDS\" AS T3 ON (T1.\"brand_id\" = T3.\"brand_id\")";
            Assert.Equal(expected, sql);

            query.ExecuteReader();
        }
    }
}
