using System;

namespace TokenizerLib
{
    /// <summary>
    /// Setellt ein Token in einem Ausdruck dar.
    /// </summary>
    public struct Token
    {
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public Token(string text, TokenType type, object value = null) : this()
        {
            Text = text;
            Type = type;
            Value = value ?? text;
        }

        /// <summary>
        /// Der f�r das Token geparste Text (mit allen Leerzeichen etc.).
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Der f�r Value relevante Bereich in Text
        /// </summary>
        public TokenRange SignificantPart { get; set; }

        /// <summary>
        /// Der Typ Tokens.
        /// </summary>
        public TokenType Type { get; }

        /// <summary>
        /// Wert des Tokens (je nach Typ).
        /// </summary>
        public object Value { get; set; }

        public string Identifier => Type == TokenType.Identifier || Type == TokenType.Name ? Value as string : null;

        public string Name => Type == TokenType.Name ? Value as string : null;

        public char? Separator => Type == TokenType.Separator ? Value as char? : null;

        public string Operator => Type == TokenType.Operator ? Value as string : null;

        /// <summary>
        /// Vereinfachte Weise einen Identifier zu �berpr�fen.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public bool IsIdentifier(string name, StringComparison comparison = StringComparison.Ordinal)
        {
            if (Type != TokenType.Identifier && Type != TokenType.Name)
            {
                return false;
            }

            return string.Compare(Value as string, name, comparison) == 0;
        }

        public bool IsName(string name, StringComparison comparison = StringComparison.Ordinal)
        {
            if (Type != TokenType.Name)
            {
                return false;
            }

            return string.Compare(Value as string, name, comparison) == 0;
        }

        public bool IsKeyword(string name, StringComparison comparison = StringComparison.Ordinal)
        {
            if (Type != TokenType.Identifier)
            {
                return false;
            }

            return string.Compare(Value as string, name, comparison) == 0;
        }

        public bool IsSeparator(char sep)
        {
            return Type == TokenType.Separator && (char)Value == sep;
        }

        public bool IsOperator(string name)
        {
            return Type == TokenType.Operator && Value as string == name;
        }

        public static bool operator ==(Token first, Token second)
        {
            return first.Equals(second);
        }

        public static bool operator !=(Token first, Token second)
        {
            return !(first == second);
        }

        public override bool Equals(System.Object obj)
        {
            if (obj is Token token)
            {
                return Equals(token);
            }

            return false;
        }

        private bool Equals(Token token)
        {
            return Type == token.Type && Value.Equals(token.Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode() ^ Type.GetHashCode();
        }
    }
}