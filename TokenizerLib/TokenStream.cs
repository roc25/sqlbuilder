using System;
using System.Linq;

namespace TokenizerLib
{
    public class TokenStream
    {
        public Token[] TokenList { get; }

        public int Pos { get; private set; } = -1;

        public bool AtEnd => Pos == -1;

        public bool IsEmpty => TokenList.Length == 0;

        public Token Current => Pos == -1 ? new Token() : TokenList[Pos];

        public Token Previous => Pos == 0 ? new Token() : Pos > 0 ? TokenList[Pos - 1] : TokenList.Length > 0 ? TokenList[TokenList.Length - 1] : new Token();

        public TokenStream(Token[] tokenList)
        {
            TokenList = tokenList ?? throw new ArgumentNullException(nameof(tokenList));
        }

        public bool MoveNext()
        {
            Pos++;

            if (Pos < TokenList.Length)
            {
                return true;
            }

            Pos = -1;
            return false;

        }

        public bool MovePrev()
        {
            if (Pos == -1)
            {
                Pos = TokenList.Length;
            }

            Pos--;

            return Pos != -1;
        }

        public TokenStream Skip(int count = 1)
        {
            if (count < 1 || Pos == -1)
            {
                return this;
            }

            Pos += count;

            if (Pos < -1 || Pos >= TokenList.Length)
            {
                Pos = -1;
            }

            return this;
        }

        public Token Read()
        {
            if (Pos == -1)
            {
               return new Token();
            }

            var token = Current;

            MoveNext();

            return token;
        }

        public Token ReadNext()
        {
            MoveNext();
            return Read();
        }

        public TokenStream Read(int count)
        {
            if (Pos == -1)
            {
                return new TokenStream(Array.Empty<Token>());
            }

            if (Pos + count > TokenList.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count), count, "Not enough elements available to read.");
            }

            var result = new Token[count];
            Array.Copy(TokenList, Pos, result, 0, count);

            Pos += count;
            if (Pos == TokenList.Length)
            {
                Pos = -1;
            }

            return new TokenStream(result);
        }

        public TokenStream ReadToEnd()
        {
            return Read(TokenList.Length - Pos);
        }

        public TokenStream GetRange(int start, int length)
        {
            if (start < 0)
            {
                start += TokenList.Length;
            }

            if (start < 0 || start > TokenList.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(start));
            }

            if (length < 0)
            {
                length += 1 + TokenList.Length;
            }

            if (length < 0 || start + length > TokenList.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            var result = new Token[length];
            Array.Copy(TokenList, start, result, 0, length);

            return new TokenStream(result);
        }

        public override string ToString()
        {
            return string.Join(string.Empty, TokenList.Select(t => t.Text));
        }
    }
}