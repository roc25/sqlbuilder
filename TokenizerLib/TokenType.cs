namespace TokenizerLib
{
    public enum TokenType
    {
        Unknown,
        Identifier,
        Separator,
        Operator,
        Integer,
        Decimal,
        String,
        Comment,
        Name, // Token is an identifier but not a keyword (identifier placed in square brackets for SQL or preceded with an ampersand in CSharp)
    }
}
