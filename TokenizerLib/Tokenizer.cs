using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TokenizerLib
{
    /// <summary>
    /// Zerlegt einen Stringausdruck in einzelne Token.
    /// Der Tokenizer ist sehr primitiv und verwendet nur 2 Zeichengruppen: Trennzeichen (SeparatorChars), Operatoren (OperatorChars) und alle Nicht-Trennzeichen.
    /// Den Nicht-Trennzeichen Token (den Namen) wird dann noch eine Bedeutung zugeordnet (Typ).
    /// Eine Ausnahme bilden Leerzeichen. Sie gelten als Trennzeichen werden aber nicht als Token ausgegeben.
    /// Zu den Leerzeichen geh�ren auch Kommentare, wenn IgnoreComments auf True steht!
    /// Kommentare gehen im SQL-Mode von -- bis Zeilenende und zwischen /**/. Im CSharp-Mode sind es // bis Zeilenende und /**/.
    /// Alle Kommentare enden auch beim Kommentar-Ende-Zeichen falls es definiert ist. Dies ist ein Sonderfall f�r List&Label.
    /// Eine andere Ausnahme bei Trennzeichen sind " und '. Sie kennzeichnen den Beginn (und das Ende) eines Strings und werden nicht als Separatoren ausgegeben.
    /// Alle folgenden Zeichen bis zum n�chsten " bzw. ' geh�ren zum String-Token (auch Kommentar-Ende-Zeichen wie z.B. Chevrons).
    /// In einem String k�nnen einzelne " bzw. ' durch verdoppelung eingef�gt werden. Beispiele:
    /// """" ergibt: "
    /// "Hallo ""Welt""" ergibt: Hallo "Welt"
    /// Identifier die als long (64-Bit Zahl) interpretierbar sind erhalten den Typ Integer.
    /// Beispiel:
    /// Der Ausdruck " Eine    Funktion( \"Hallo\" , 5 )" wird in folgende {Token.Text, Token.Type, Token.Value} zerlegt:
    ///  {{" Eine", Identifier, "Eine"},
    ///  {{"    Funktion", Identifier, "Funktion"},
    ///  {"(", Separator, "("},
    ///  {" \"Hallo\"", String, "Hallo"},
    ///  {" ,", Separator, ","},
    ///  {" 5", Integer, 5L},
    ///  {" )", Separator, ")"}}
    /// </summary>
    public class Tokenizer
    {
        private enum CommentTypeEnd
        {
            None, // Kein Kommentar
            Line, // Kommentar geht bis zum Zeilenende
            CStyle, // Kommentar h�rt mit */ auf
            CStyleSkipChar // Kommentar h�rt mit */ auf aber erst beim n�chsten Zeichen
        }

        /// <summary>
        /// Aktuelle Position im Ausdruck (immer nach dem zuletzt geparsten Token)
        /// </summary>
        private int _pos;

        private Token _token;

        /// <summary>
        /// Ausdruck der geparst werden soll
        /// </summary>
        private string _expression;

        private readonly List<InterpolatedStringProcessingInfo> _interpolatedStringProcessingInfo = new List<InterpolatedStringProcessingInfo>();

        public enum Mode
        {
            SQL,
            CSharp
        }

        public enum CommentParseMode
        {
            SqlStyle,
            CStyle
        }

        public enum StringParseMode
        {
            SqlStyle,
            CStyle,
            CSharpStyle,
            MinimalCStyle, // Hack to parse legacy strings. It will skip escaped string delimiters only.
            CustomStyle, // For strings starting with "$:". GetRawString() must be implemented in a derived class.
        }

        public enum IdentifierEscapeMode
        {
            SqlStyle,
            CSharpStyle
        }

        /// <summary>
        /// Array of operator characters.
        /// Operator characters are **cumulative**.
        /// </summary>
        public char[] OperatorChars { get; set; } = { '!', '#', '$', '%', '&', '*', '+', '-', '.', '/', ':', '<', '=', '>', '?', '@', '\\', '^', '|', '~' };

        /// <summary>
        /// Array of separator characters.
        /// Each character is a separator.
        /// ' and " are never returned as separators.
        /// </summary>
        public char[] SeparatorChars { get; set; } = { '"', '\'', '(', ')', ',', ';', '[', ']', '{', '}', '�', '�' };

        public string Expression
        {
            get => _expression;
            set
            {
                _pos = 0;
                _expression = value;
                _token = new Token();
            }
        }

        public CommentParseMode CommentMode { get; set; }

        public StringParseMode StringMode { get; set; }

        public IdentifierEscapeMode IdentifierMode { get; set; }

        public bool IgnoreComments { get; set; }

        /// <summary>
        /// Ends the parsing unconditionally.
        /// Must be one of the SeparatorChars (a chevron for example).
        /// </summary>
        public char ParserEndChar { get; set; }

        public Token Token => _token;

        public int InterpolatedStringLevel => _interpolatedStringProcessingInfo.Count;

        /// <summary>
        /// Aktuelle Position des Parsers im String
        /// </summary>
        public int Pos
        {
            get => _pos;
            set
            {
                _pos = value;
                _token = new Token();
            }
        }

        public static TokenStream Parse(string expression, Mode parseMode = Mode.SQL)
        {
            var tokenList = new List<Token>();

            var tokenizer = new Tokenizer(expression, parseMode);

            while (tokenizer.Next())
            {
                tokenList.Add(tokenizer.Token);
            }

            return new TokenStream(tokenList.ToArray());
        }

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="mode"></param>
        public Tokenizer(string expression = "", Mode parseMode = Mode.SQL)
        {
            _expression = expression;
            switch (parseMode)
            {
                case Mode.SQL:
                    CommentMode = CommentParseMode.SqlStyle;
                    StringMode = StringParseMode.SqlStyle;
                    IdentifierMode = IdentifierEscapeMode.SqlStyle;
                    break;
                case Mode.CSharp:
                    CommentMode = CommentParseMode.CStyle;
                    StringMode = StringParseMode.CSharpStyle;
                    IdentifierMode = IdentifierEscapeMode.CSharpStyle;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            IgnoreComments = true;
        }

        public Token NextToken()
        {
            Next();
            return _token;
        }

        /// <summary>
        /// Parst das n�chste Token.
        /// </summary>
        /// <returns></returns>
        public bool Next()
        {
            _token = new Token();

            if (_expression == null || _pos >= _expression.Length)
            {
                if (!FetchMore())
                {
                    return false;
                }
            }

            var stringVal = new StringBuilder();
            var stringEndChar = '\0'; // Zeichen mit dem eine Zeichenkette beendet wird
            var identifierEndChar = '\0';
            var isStringEscapeCStyle = StringMode != StringParseMode.SqlStyle; // Default string parsing mode for the next token. May be changed for CSharpStyle.
            var isInterpolatedString = false;
            var prev = '\0'; // Das vorhergehende Zeichen
            var tokenType = TokenType.Unknown;
            var numVal = 0M;
            var magnitude = 0;
            var commentType = CommentTypeEnd.None;
            var begin = _pos;
            var tokenStart = _pos; // Beginn des relevanten Teils f�r Value (nach Leerzeichen und Kommentaren)

            // Zeichen f�r Zeichen analysieren bis das n�chste Token vollst�ndig ist.
            while (_pos < _expression.Length)
            {
                var ch = _expression[_pos];

                if (commentType != CommentTypeEnd.None)
                {
                    // ==> TokenType == TokenType.Comment

                    // The ParserEndChar (for example a closing chevron) ends all comments
                    if (ch == ParserEndChar)
                    {
                        commentType = CommentTypeEnd.None;
                        if (IgnoreComments)
                        {
                            // Use parsed character as token
                            stringVal.Clear();
                            tokenType = TokenType.Separator;
                            tokenStart = _pos;
                            stringVal.Append(ch); // ParserEndChar
                            _pos++;
                        }
                        break;
                    }

                    // Check for normal comment terminator
                    switch (commentType)
                    {
                        case CommentTypeEnd.Line:
                            if (ch == '\n')
                            {
                                commentType = CommentTypeEnd.None;
                            }
                            else
                            {
                                stringVal.Append(ch);
                            }

                            break;
                        case CommentTypeEnd.CStyle:
                            if (prev == '*' && ch == '/')
                            {
                                commentType = CommentTypeEnd.None;
                            }
                            else
                            {
                                stringVal.Append(prev);
                            }

                            break;
                        case CommentTypeEnd.CStyleSkipChar:
                            commentType = CommentTypeEnd.CStyle; // Auf das n�chste Zeichen warten
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    if (commentType == CommentTypeEnd.None && IgnoreComments)
                    {
                        tokenType = TokenType.Unknown;
                        stringVal.Clear();
                        // Continue parsing
                    }
                }
                else if (tokenType == TokenType.Comment)
                {
                    // ==> commentType == CommentTypeEnd.None
                    // _pos is one character after the comment.
                    break;
                }
                else if (tokenType == TokenType.String)
                {
                    if (stringEndChar != '\0')
                    {
                        if (isStringEscapeCStyle && prev == '\\')
                        {
                            if (StringMode == StringParseMode.MinimalCStyle)
                            {
                                if (ch == stringEndChar)
                                {
                                    stringVal[stringVal.Length - 1] = ch;
                                    ch = '\0'; // Das hinzugef�gte Zeichen wird nicht mit Folgezeichen kombiniert
                                }
                                else
                                {
                                    stringVal.Append(ch == '�' ? '\n' : ch);
                                }
                            }
                            else
                            {
                                var escIndex = "ntvbrfa\\'\"0".IndexOf(ch);

                                if (escIndex != -1)
                                {
                                    ch = "\n\t\v\b\r\f\a\\\'\"\0"[escIndex];
                                }

                                stringVal[stringVal.Length - 1] = ch;
                                ch = '\0'; // Das hinzugef�gte Zeichen wird nicht mit Folgezeichen kombiniert
                            }
                        }
                        else if (ch == stringEndChar)
                        {
                            stringEndChar = '\0';
                        }
                        else if (isInterpolatedString && ch == '{')
                        {
                            _interpolatedStringProcessingInfo.Add(new InterpolatedStringProcessingInfo { StringEndChar = stringEndChar, StringEscapeCStyle = isStringEscapeCStyle, BraceDepth = 0 });
                            stringEndChar = '\0';
                        }
                        else if (isInterpolatedString && prev == '}' && ch == '}' && stringVal.Length > 0)
                        {
                            // Ignore this character
                            ch = '\0'; // Avoid to combine again
                        }
                        else
                        {
                            stringVal.Append(ch == '�' ? '\n' : ch);
                        }
                    }
                    else
                    {
                        // Check if the string is continued
                        if (!isStringEscapeCStyle && prev == ch)
                        {
                            stringVal.Append(ch);
                            stringEndChar = ch;
                        }
                        // Check for possible escape of open curly brace in interpolated strings
                        else if (isInterpolatedString && ch == '{' && prev == '{')
                        {
                            stringVal.Append(ch);
                            stringEndChar = _interpolatedStringProcessingInfo.Last().StringEndChar;
                            _interpolatedStringProcessingInfo.RemoveAt(_interpolatedStringProcessingInfo.Count - 1);
                        }
                        else
                        {
                            // This is definitely the end of the string
                            break;
                        }
                    }
                }
                else if (tokenType == TokenType.Name)
                {
                    if (identifierEndChar == '\0')
                    {
                        if (prev == ch)
                        {
                            stringVal.Append(ch);
                            identifierEndChar = ch;
                        }
                        else
                        {
                            // This is definitely the end of the identifier
                            break;
                        }
                    }
                    else if (ch == identifierEndChar)
                    {
                        identifierEndChar = '\0';
                    }
                    else
                    {
                        stringVal.Append(ch);
                    }
                }
                else if (tokenType == TokenType.Identifier)
                {
                    if (SeparatorChars.Contains(ch) || OperatorChars.Contains(ch) || IsWhiteSpace(ch))
                    {
                        break;
                    }

                    stringVal.Append(ch);
                }
                else if (tokenType == TokenType.Integer || tokenType == TokenType.Decimal)
                {
                    if (ch == '.')
                    {
                        if (tokenType == TokenType.Decimal)
                        {
                            break;
                        }

                        tokenType = TokenType.Decimal;
                    }
                    else
                    {
                        if (!char.IsDigit(ch))
                        {
                            break;
                        }

                        numVal *= 10;
                        numVal += (int)char.GetNumericValue(ch);
                        if (tokenType == TokenType.Decimal)
                        {
                            magnitude *= 10;
                        }
                    }
                }
                else if (_interpolatedStringProcessingInfo.LastOrDefault()?.BraceDepth == 0 && (ch == ':' || ch == '}'))
                {
                    if (tokenType == TokenType.Operator)
                    {
                        // There is a preceding operator
                        break;
                    }

                    // The colon starts the format section of an interpolated string!
                    // Continue parsing the string
                    tokenType = TokenType.String;
                    tokenStart = _pos;
                    isStringEscapeCStyle = _interpolatedStringProcessingInfo.Last().StringEscapeCStyle;
                    isInterpolatedString = true;
                    stringEndChar = _interpolatedStringProcessingInfo.Last().StringEndChar;
                    _interpolatedStringProcessingInfo.RemoveAt(_interpolatedStringProcessingInfo.Count - 1);
                }
                else if (SeparatorChars.Contains(ch))
                {
                    if (ch == '\'' || ch == '"')
                    {
                        if (tokenType == TokenType.Operator && StringMode == StringParseMode.CSharpStyle)
                        {
                            var pos = stringVal.Length;

                            if (stringVal[pos - 1] == '@')
                            {
                                pos--;
                                isStringEscapeCStyle = false;
                                if (pos > 0 && stringVal[pos - 1] == '$')
                                {
                                    pos--;
                                    isInterpolatedString = true;
                                }
                            }
                            else if (stringVal[pos - 1] == '$')
                            {
                                pos--;
                                isInterpolatedString = true;
                                if (pos > 0 && stringVal[pos - 1] == '@')
                                {
                                    pos--;
                                    isStringEscapeCStyle = false;
                                }
                            }

                            if (pos > 0)
                            {
                                // There is a preceding operator
                                _pos -= stringVal.Length - pos;
                                stringVal.Length = pos;
                                break;
                            }

                            stringVal.Clear();
                            tokenType = TokenType.String;
                            //tokenStart = _pos; // Leave tokenStart where the operator started
                            stringEndChar = ch;
                        }
                        else if (tokenType == TokenType.Unknown)
                        {
                            tokenType = TokenType.String;
                            tokenStart = _pos;
                            stringEndChar = ch;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (tokenType != TokenType.Unknown)
                        {
                            break;
                        }

                        if (IdentifierMode == IdentifierEscapeMode.SqlStyle && ch == '[')
                        {
                            tokenType = TokenType.Name;
                            identifierEndChar = ']';
                            tokenStart = _pos;
                        }
                        else
                        {
                            if (_interpolatedStringProcessingInfo.Any())
                            {
                                if (ch == '{' || ch == '(')
                                {
                                    _interpolatedStringProcessingInfo.Last().BraceDepth++;
                                }
                                else if (ch == '}' || ch == ')')
                                {
                                    _interpolatedStringProcessingInfo.Last().BraceDepth--;
                                }
                            }

                            tokenType = TokenType.Separator;
                            tokenStart = _pos;
                            stringVal.Append(ch);
                            _pos++;

                            break;
                        }
                    }
                }
                else if (tokenType == TokenType.Operator) // Operators end immediately on a non-operator character including white space!
                {
                    // Check if the operator has a special meaning for the parser
                    if (StringMode == StringParseMode.CustomStyle && prev == '$' && ch == ':')
                    {
                        // Special case for string parsing (for example in Rich-Text editors)
                        // GetRawString() must be implemented in a derived class

                        if (stringVal.Length > 1)
                        {
                            // There is a preceding operator
                            stringVal.Length--;
                            _pos--;
                            break;
                        }

                        Debug.Assert(_pos + 1 == _expression.Length);

                        tokenType = TokenType.String;
                        tokenStart = _pos - 1;
                        stringVal.Append(GetRawString() ?? string.Empty);
                        _pos = _expression.Length - 1;
                    }
                    else
                    {
                        // Auf Kommentar pr�fen!
                        if (CommentMode == CommentParseMode.SqlStyle)
                        {
                            if (prev == '-' && ch == '-')
                            {
                                commentType = CommentTypeEnd.Line;
                            }
                        }
                        else if (CommentMode == CommentParseMode.CStyle)
                        {
                            if (prev == '/' && ch == '/')
                            {
                                commentType = CommentTypeEnd.Line;
                            }
                        }

                        if (prev == '/' && ch == '*')
                        {
                            commentType = CommentTypeEnd.CStyleSkipChar; // Es muss mindestens ein Zeichen gewartet werden bevor auf das Ende gepr�ft werden darf.
                        }

                        if (commentType != CommentTypeEnd.None)
                        {
                            if (stringVal.Length > 1)
                            {
                                // There is a preceding operator
                                stringVal.Length--;
                                _pos--;
                                break;
                            }

                            tokenType = TokenType.Comment;
                            tokenStart = _pos - 1; // Jede Art von Kommentar startet mit dem vorhergehenden Zeichen
                            stringVal.Clear();
                        }
                        else
                        {
                            if (!OperatorChars.Contains(ch))
                            {
                                break;
                            }

                            stringVal.Append(ch);
                        }
                    }
                }
                else if (OperatorChars.Contains(ch))
                {
                    if (tokenType != TokenType.Unknown)
                    {
                        break;
                    }

                    tokenType = TokenType.Operator;
                    tokenStart = _pos;
                    stringVal.Append(ch);
                }
                else if (char.IsDigit(ch))
                {
                    if (tokenType != TokenType.Unknown)
                    {
                        break;
                    }

                    numVal = (int) char.GetNumericValue(ch);
                    magnitude = 1;
                    tokenType = TokenType.Integer;
                    tokenStart = _pos;
                }
                else if (!IsWhiteSpace(ch)) // Alle Leerzeichen ignorieren
                {
                    if (tokenType != TokenType.Unknown)
                    {
                        break;
                    }

                    tokenType = TokenType.Identifier;
                    tokenStart = _pos;
                    stringVal.Append(ch);
                }

                prev = ch;
                _pos++;

                if (_pos == _expression.Length)
                {
                    FetchMore();
                }
            }

            _token = new Token(_expression.Substring(begin, _pos - begin), tokenType)
            {
                SignificantPart = TokenRange.RelativeRange(tokenStart - begin, _pos - tokenStart)
            };

            switch (tokenType)
            {
                case TokenType.Integer:
                    _token.Value = (long)numVal;
                    break;
                case TokenType.Decimal:
                    _token.Value = numVal / magnitude;
                    break;
                case TokenType.Separator:
                    _token.Value = stringVal[0];
                    break;
                default:
                    _token.Value = stringVal.ToString();
                    break;
            }

            return tokenType != TokenType.Unknown;
        }

        /// <summary>
        /// Used to extend the _expression in FetchMore()
        /// </summary>
        /// <param name="expression"></param>
        protected void ReplaceExpression(string expression)
        {
            _expression = expression;
        }

        protected virtual bool FetchMore()
        {
            // Only extend _expression.
            return false;
        }

        protected virtual string GetRawString()
        {
            return string.Empty;
        }

        /// <summary>
        /// Pr�ft ob <paramref name="ch"/> WhiteSpace ist.
        /// Diese Methode pr�ft auch auf das '�' und '�' zeichen, welches Whitespace ist aber nicht mit <see cref="char.IsWhiteSpace(char)"/> gefunden wird.
        /// </summary>
        /// <param name="ch"></param>
        /// <returns></returns>
        private static bool IsWhiteSpace(char ch)
        {
            return char.IsWhiteSpace(ch) || Equals('�', ch) || Equals('�', ch);
        }

        private class InterpolatedStringProcessingInfo
        {
            public char StringEndChar;
            public bool StringEscapeCStyle;
            public int BraceDepth; // Number of open curly and round braces
        }
    }
}