﻿namespace TokenizerLib
{
    public struct TokenRange
    {
        public static TokenRange AbsoluteRange(long start, long end)
        {
            return new TokenRange
            {
                Start = start,
                Length = end - start
            };
        }

        public static TokenRange RelativeRange(long start, long length)
        {
            return new TokenRange
            {
                Start = start,
                Length = length
            };
        }

        public long Start;
        public long Length;

        public bool IsEmpty => Length == 0;

        public long End => Start + Length;

        public bool Contains(long value)
        {
            return value >= Start && value < Start + Length;
        }
    }
}
