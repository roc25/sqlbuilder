﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SqlBuilderFramework
{
    public class SqlBuilderQuery<TT> : SqlBuilderBase<TT> where TT : DbTable
    {
        private bool _distinct;
        private readonly List<KeyValuePair<IDbColumnImpl, bool>> _orderList = new List<KeyValuePair<IDbColumnImpl, bool>>();
        private int _offset;
        private int _limit;

        public override StatementType StatementType => StatementType.Select;

        public DbConstraint Constraint { get; private set; }

        public bool IsOrdered => _orderList.Any();

        public SqlBuilderQuery(TT source, IDatabase database = null)
            : base(source, database)
        {
        }

        public SqlBuilderQuery<TT> Distinct(bool distinct = true)
        {
            _distinct = distinct;
            return this;
        }

        public SqlBuilderQuery<TT> Offset(int offset)
        {
            _offset = offset;
            return this;
        }

        public SqlBuilderQuery<TT> Limit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SqlBuilderQuery<TT> Where(DbConstraint constraint)
        {
            Constraint = constraint;
            return this;
        }

        public SqlBuilderQuery<TT> And(DbConstraint constraint)
        {
            if (Constraint == null)
                return Where(constraint);

            Constraint &= constraint;
            return this;
        }

        public SqlBuilderQuery<TT> Or(DbConstraint constraint)
        {
            if (Constraint == null)
                return Where(constraint);

            Constraint |= constraint;
            return this;
        }

        public SqlBuilderQuery<TT> OrderBy(params DbColumn[] columns) // Ascending
        {
            return OrderAscending(columns);
        }

        public SqlBuilderQuery<TT> OrderAscending(params DbColumn[] columns)
        {
            foreach (var column in columns)
            {
                if ((object)column != null)
                    _orderList.Add(new KeyValuePair<IDbColumnImpl, bool>(column.DbColumnImpl, true));
            }
            return this;
        }

        public SqlBuilderQuery<TT> OrderDescending(params DbColumn[] columns)
        {
            foreach (var column in columns)
            {
                if ((object)column != null)
                    _orderList.Add(new KeyValuePair<IDbColumnImpl, bool>(column.DbColumnImpl, false));
            }
            return this;
        }

        public SqlBuilderQuery<TT> Returning(params DbColumn[] columns)
        {
            ColumnBindings.AddRange(columns.Select(c => new OutputColumn { Column = c.DbColumnImpl }));
            return this;
        }

        public SqlBuilderQuery<TT> Map<T>(DbTypedColumn<T> column, Action<T> action)
        {
            Bind(column).Setter = action;
            return this;
        }

        public List<TC> ReadAll<TC>(IDatabase database) where TC : new()
        {
            Database = database;
            return ReadInto<TC>();
        }

        public List<TC> ReadInto<TC>() where TC : new()
        {
            return base.ReadAll(CreateMapper<TC>());
        }

        public List<Tuple<TC1, TC2>> ReadAll<TC1, TC2>(IDatabase database) where TC1 : new() where TC2 : new()
        {
            Database = database;
            return ReadInto<TC1, TC2>();
        }

        public List<Tuple<TC1, TC2>> ReadInto<TC1, TC2>() where TC1 : new() where TC2 : new()
        {
            return base.ReadAll(CreateMapper<TC1>(), CreateMapper<TC2>());
        }

        #region Conversions

        public DbInlineTable ToTable()
        {
            return new DbInlineTable(this);
        }

        public DbInlineTable<T> ToTable<T>(T columns)
        {
            ColumnBindings.Clear();

            var dbColumns = columns.GetType().GetProperties()
                .Where(p => p.PropertyType.IsSubclassOf(typeof(DbColumn)))
                .Select(p => p.GetValue(columns) as DbColumn)
                .Where(c => (object)c != null)
                .ToArray();

            ColumnBindings.AddRange(dbColumns.Select(c => new OutputColumn { Column = c.DbColumnImpl }));

            return new DbInlineTable<T>(this, dbColumns, columns.GetType());
        }

        public DbConstraint Exists(DbColumn column)
        {
            ColumnBindings.Clear();
            ColumnBindings.Add(new OutputColumn { Column = column.DbColumnImpl });
            return new DbConstraint(null, "EXISTS", new DbColumn(new DbQueryColumn(this)));
        }

        #endregion

        public override string CreateSql(BuilderContext builderContext, bool usePlaceholder)
        {
            var sql = new StringBuilder();

            var fromClause = Table.Definition(builderContext, usePlaceholder);
            sql.Append("SELECT ");
            if (_distinct)
                sql.Append("DISTINCT ");
            sql.Append(string.Join(", ", builderContext.OutputColumns.Select(column => column.Definition(builderContext, usePlaceholder))));
            sql.Append(" FROM ");
            sql.Append(fromClause);
            if (Constraint != null)
            {
                sql.Append(" WHERE ");
                sql.Append(Constraint.Sql(builderContext, usePlaceholder));
            }
            if (builderContext.OutputColumns.Any(column => column.IsAggregate))
            {
                var groupClause = string.Join(", ", builderContext.OutputColumns.Where(column => column.IsGroupable).Select(binding => binding.Sql(builderContext, usePlaceholder)));

                if (!string.IsNullOrEmpty(groupClause))
                {
                    sql.Append(" GROUP BY ");
                    sql.Append(groupClause);
                }
            }
            if (_orderList.Any())
            {
                sql.Append(" ORDER BY ");
                sql.Append(string.Join(", ", _orderList.Select(order => (builderContext.Alias(order.Key) ?? order.Key.Sql(builderContext, usePlaceholder)) + (order.Value ? "" : " DESC"))));
                if (Database.Provider == DatabaseProvider.Sqlite)
                {
                    if (_limit > 0)
                    {
                        sql.Append(" LIMIT ");
                        sql.Append(_limit);
                        if (_offset > 0)
                        {
                            sql.Append(" OFFSET ");
                            sql.Append(_offset);
                        }
                    }
                }
                else
                {
                    if (_offset > 0 || _limit > 0)
                    {
                        sql.Append(" OFFSET ");
                        sql.Append(_offset);
                        sql.Append(" ROWS");
                    }
                    if (_limit > 0)
                    {
                        sql.Append(" FETCH NEXT ");
                        sql.Append(_limit);
                        sql.Append(" ROWS ONLY");
                    }
                }
            }

            return sql.ToString();
        }

        private DbMapper<TC> CreateMapper<TC>() where TC : new()
        {
            var mapper = new DbMapper<TC>();

            // Add mappings from fields with DbColumnAttribute
            foreach (var fieldInfo in typeof(TC).GetFields())
            {
                var annotation = fieldInfo.GetCustomAttribute(typeof(DbColumnAttribute)) as DbColumnAttribute;

                var column = annotation?.GetColumnFromAnnotation(Table, fieldInfo.Name);

                if (column is object)
                {
                    mapper.Map<TC>(column, (model, value) => fieldInfo.SetValue(model, DbValueConverter.ChangeType(value, fieldInfo.FieldType)));
                }
            }

            // Add mappings from properties with DbColumnAttribute
            foreach (var propertyInfo in typeof(TC).GetProperties())
            {
                var annotation = propertyInfo.GetCustomAttribute(typeof(DbColumnAttribute)) as DbColumnAttribute;

                var column = annotation?.GetColumnFromAnnotation(Table, propertyInfo.Name);

                if (column is object)
                {
                    mapper.Map<TC>(column, (model, value) => propertyInfo.SetValue(model, DbValueConverter.ChangeType(value, propertyInfo.PropertyType)));
                }
            }

            // Add mappings from methods returning a DbMapper<TC>
            foreach (var methodInfo in typeof(TC).GetMethods(BindingFlags.Static | BindingFlags.Public))
            {
                var returnType = methodInfo.ReturnType;

                if (returnType != typeof(DbMapper<TC>))
                {
                    continue;
                }

                var parameterInfos = methodInfo.GetParameters();
                var parameters = new List<object>();

                foreach (var parameterInfo in parameterInfos)
                {
                    var parameterType = parameterInfo.ParameterType;

                    if (!typeof(DbTable).IsAssignableFrom(parameterType))
                    {
                        break;
                    }

                    var tableSource = Table.Get(parameterType);

                    if (tableSource == null && !parameterInfo.HasDefaultValue)
                    {
                        break;
                    }

                    parameters.Add(tableSource);
                }

                if (parameters.Count() == parameterInfos.Length)
                {
                    mapper.AddEntries((DbMapper<TC>)methodInfo.Invoke(null, parameters.ToArray()));
                }
            }

            return mapper;
        }
    }
}
