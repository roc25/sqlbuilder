﻿using System.Collections.Generic;

namespace SqlBuilderFramework
{
    public interface IDbInsertAssignments<T>
    {
        IEnumerable<DbAssignment> GetAssignments(T table);
    }
}
