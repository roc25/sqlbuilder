﻿using System.Collections.Generic;
using System.Data;

namespace SqlBuilderFramework
{
    public interface ISqlBuilder
    {
        StatementType StatementType { get; }

        DbBoundValue Bind(DbColumn column);

        DbTypedBoundValue<T> Bind<T>(DbTypedColumn<T> column);

        ISqlBuilderReader ExecuteReader(IDatabase database);

        int ExecuteNonQuery(IDatabase database);

        void SetValues(DbResultSet resultSet);

        string Sql(List<ParameterEntity> parameterList, IDatabase database = null);

        string Sql(BuilderContext parentContext, bool usePlaceholder, out BuilderContext builderContext);

        string CreateSql(BuilderContext builderContext, bool usePlaceholder);
    }
}
