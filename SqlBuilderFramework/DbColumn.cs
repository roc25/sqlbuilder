﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SqlBuilderFramework
{
    public interface IDbColumnImpl
    {
        Type Type { get; set; }
        DbTable Table { get; }
        bool IsAggregate { get; }
        bool IsGroupable { get; }
        string Sql(BuilderContext builderContext, bool usePlaceholder);
        string Definition(BuilderContext builderContext, bool usePlaceholder);
    }

    public class DbColumn
    {
        internal IDbColumnImpl DbColumnImpl;

        public Type Type => DbColumnImpl?.Type;
        public bool IsAggregate => DbColumnImpl?.IsAggregate ?? false;
        public bool IsGroupable => DbColumnImpl?.IsGroupable ?? false;

        public DbColumn(IDbColumnImpl dbColumnImpl)
        {
            DbColumnImpl = dbColumnImpl;
        }

        //
        // Comparison operators
        //

        public static DbConstraint operator ==(DbColumn left, DbColumn right)
        {
            return new DbConstraint(left, "=", right);
        }

        public static DbConstraint operator !=(DbColumn left, DbColumn right)
        {
            return new DbConstraint(left, "<>", right);
        }

        public static DbConstraint operator ==(DbColumn left, bool right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<bool>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, bool right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<bool>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, int right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<int>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, int right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<int>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, long right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<long>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, long right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<long>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, decimal right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, decimal right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, DateTime right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, DateTime right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, string right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<string>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, string right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<string>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, byte[] right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, byte[] right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, Guid right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<Guid>(right)));
        }

        public static DbConstraint operator !=(DbColumn left, Guid right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<Guid>(right)));
        }

        public static DbConstraint operator ==(DbColumn left, Enum right)
        {
            return new DbConstraint(left, "=", new DbColumn(new DbValueColumn<int>(Convert.ToInt32(right))));
        }

        public static DbConstraint operator !=(DbColumn left, Enum right)
        {
            return new DbConstraint(left, "<>", new DbColumn(new DbValueColumn<int>(Convert.ToInt32(right))));
        }

        public DbConstraint IsNull()
        {
            return new DbConstraint(this, "=", new DbColumn(new DbValueColumn<object>(null)));
        }

        public DbConstraint IsNotNull()
        {
            return new DbConstraint(this, "<>", new DbColumn(new DbValueColumn<object>(null)));
        }

        public static DbConstraint operator <(DbColumn left, DbColumn right)
        {
            return new DbConstraint(left, "<", right);
        }

        public static DbConstraint operator >(DbColumn left, DbColumn right)
        {
            return new DbConstraint(left, ">", right);
        }

        public static DbConstraint operator <(DbColumn left, bool right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<bool>(right)));
        }

        public static DbConstraint operator >(DbColumn left, bool right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<bool>(right)));
        }

        public static DbConstraint operator <(DbColumn left, int right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<int>(right)));
        }

        public static DbConstraint operator >(DbColumn left, int right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<int>(right)));
        }

        public static DbConstraint operator <(DbColumn left, long right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<long>(right)));
        }

        public static DbConstraint operator >(DbColumn left, long right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<long>(right)));
        }

        public static DbConstraint operator <(DbColumn left, decimal right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public static DbConstraint operator >(DbColumn left, decimal right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public static DbConstraint operator <(DbColumn left, DateTime right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public static DbConstraint operator >(DbColumn left, DateTime right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public static DbConstraint operator <(DbColumn left, string right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<string>(right)));
        }

        public static DbConstraint operator >(DbColumn left, string right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<string>(right)));
        }

        public static DbConstraint operator <(DbColumn left, byte[] right)
        {
            return new DbConstraint(left, "<", new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public static DbConstraint operator >(DbColumn left, byte[] right)
        {
            return new DbConstraint(left, ">", new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, DbColumn right)
        {
            return new DbConstraint(left, "<=", right);
        }

        public static DbConstraint operator >=(DbColumn left, DbColumn right)
        {
            return new DbConstraint(left, ">=", right);
        }

        public static DbConstraint operator <=(DbColumn left, bool right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<bool>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, bool right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<bool>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, int right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<int>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, int right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<int>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, long right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<long>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, long right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<long>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, decimal right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, decimal right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, DateTime right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, DateTime right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, string right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<string>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, string right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<string>(right)));
        }

        public static DbConstraint operator <=(DbColumn left, byte[] right)
        {
            return new DbConstraint(left, "<=", new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public static DbConstraint operator >=(DbColumn left, byte[] right)
        {
            return new DbConstraint(left, ">=", new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public DbConstraint Like(string expression)
        {
            return new DbConstraint(this, "LIKE", new DbColumn(new DbValueColumn<string>(expression)));
        }

        public DbConstraint NotLike(string expression)
        {
            return new DbConstraint(this, "NOT LIKE", new DbColumn(new DbValueColumn<string>(expression)));
        }

        public DbConstraint In<T>(IEnumerable<T> intList)
        {
            return new DbConstraint(this, "IN", new DbColumn(new DbListColumn(intList.Select(cmpValue => new DbValueColumn<T>(cmpValue)))));
        }

        public DbConstraint In(params object[] cmpList)
        {
            return new DbConstraint(this, "IN", new DbColumn(new DbListColumn(cmpList.Select(cmpValue => new DbValueColumn<object>(cmpValue)))));
        }

        public DbConstraint In(ISqlBuilder query)
        {
            return new DbConstraint(this, "IN", new DbColumn(new DbQueryColumn(query)));
        }

        public DbConstraint NotIn<T>(IEnumerable<T> intList)
        {
            return new DbConstraint(this, "NOT IN", new DbColumn(new DbListColumn(intList.Select(cmpValue => new DbValueColumn<T>(cmpValue)))));
        }

        public DbConstraint NotIn(params object[] cmpList)
        {
            return new DbConstraint(this, "NOT IN", new DbColumn(new DbListColumn(cmpList.Select(cmpValue => new DbValueColumn<object>(cmpValue)))));
        }

        public DbConstraint NotIn(ISqlBuilder query)
        {
            return new DbConstraint(this, "NOT IN", new DbColumn(new DbQueryColumn(query)));
        }

        public DbConstraint HasFlags(params int[] flags)
        {
            int consFlags = 0;
            foreach (var flag in flags)
            {
                consFlags |= flag;
            }
            return new DbConstraint(new DbColumn(new DbExprColumn("{0} & {1}", false, new[] { DbColumnImpl, new DbValueColumn<int>(consFlags) })), ">", new DbColumn(new DbValueColumn<int>(0)));
        }

        //
        // Expressions
        //

        public DbColumn Max()
        {
            return new DbColumn(new DbExprColumn("MAX({0})", true, new[] { DbColumnImpl }));
        }

        public DbTypedColumn<int> Count()
        {
            return new DbTypedColumn<int>(new DbExprColumn("COUNT({0})", true, new[] { DbColumnImpl }));
        }

        public static DbColumn operator +(DbColumn left, int right)
        {
            return new DbColumn(new DbExprColumn("({0} + {1})", false, new[] { left.DbColumnImpl, new DbValueColumn<int>(right) }));
        }

        public static DbColumn operator -(DbColumn left, int right)
        {
            return new DbColumn(new DbExprColumn("({0} - {1})", false, new[] { left.DbColumnImpl, new DbValueColumn<int>(right) }));
        }

        public static DbColumn operator +(DbColumn left, long right)
        {
            return new DbColumn(new DbExprColumn("({0} + {1})", false, new[] { left.DbColumnImpl, new DbValueColumn<long>(right) }));
        }

        public static DbColumn operator -(DbColumn left, long right)
        {
            return new DbColumn(new DbExprColumn("({0} - {1})", false, new[] { left.DbColumnImpl, new DbValueColumn<long>(right) }));
        }

        public static DbColumn operator +(DbColumn left, DbColumn right)
        {
            return new DbColumn(new DbExprColumn("({0} + {1})", false, new[] { left.DbColumnImpl, right.DbColumnImpl }));
        }

        public static DbColumn operator -(DbColumn left, DbColumn right)
        {
            return new DbColumn(new DbExprColumn("({0} - {1})", false, new[] { left.DbColumnImpl, right.DbColumnImpl }));
        }

        public DbColumn ConcatWith(DbColumn column)
        {
            return new DbColumn(new DbExprColumn("CONCAT({0}, {1})", false, new[] { DbColumnImpl, column.DbColumnImpl }));
        }

        public DbTypedColumn<string> ConcatWith(string text)
        {
            return new DbTypedColumn<string>(new DbExprColumn("CONCAT({0}, {1})", false, new[] { DbColumnImpl, new DbValueColumn<string>(text) }));
        }

        public static DbTypedColumn<T> CaseWhen<T>(DbConstraint constraint, DbColumn truePart, DbColumn falsePart = null)
        {
            if (falsePart is null)
            {
                return new DbTypedColumn<T>(new DbExprColumn("CASE WHEN {0} THEN {1} END", false, new[] { new DbConditionColumn(constraint), truePart.DbColumnImpl }));
            }

            return new DbTypedColumn<T>(new DbExprColumn("CASE WHEN {0} THEN {1} ELSE {2} END", false, new[] { new DbConditionColumn(constraint), truePart.DbColumnImpl, falsePart.DbColumnImpl }));
        }

        //
        // Assignemnt
        //

        public DbAssignment ToNull()
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<object>(null)));
        }

        public DbAssignment To(DbColumn right)
        {
            return new DbAssignment(this, right);
        }

        public DbAssignment To(bool right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<bool>(right)));
        }

        public DbAssignment To(bool? right)
        {
            if (right == null)
                return ToNull();
            return new DbAssignment(this, new DbColumn(new DbValueColumn<bool>((bool)right)));
        }

        public DbAssignment To(int right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<int>(right)));
        }

        public DbAssignment To(int? right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<int?>(right)));
        }

        public DbAssignment To(long right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<long>(right)));
        }

        public DbAssignment To(long? right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<long?>(right)));
        }

        public DbAssignment To(DateTime right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<DateTime>(right)));
        }

        public DbAssignment To(DateTime? right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<DateTime?>(right)));
        }

        public DbAssignment To(decimal right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<decimal>(right)));
        }

        public DbAssignment To(decimal? right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<decimal?>(right)));
        }

        public DbAssignment To(string right)
        {
            if (right == null)
                return ToNull();
            return new DbAssignment(this, new DbColumn(new DbValueColumn<string>(right)));
        }

        public DbAssignment To(byte[] right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<byte[]>(right)));
        }

        public DbAssignment To(Guid? guid)
        {
            if (guid != null)
                return new DbAssignment(this, new DbColumn(new DbValueColumn<byte[]>(guid.Value.ToByteArray())));
            return new DbAssignment(this, new DbColumn(new DbValueColumn<object>(null)));
        }

        public DbAssignment To(Enum right)
        {
            return new DbAssignment(this, new DbColumn(new DbValueColumn<int>(Convert.ToInt32(right))));
        }

        //
        // Helper
        //

        internal virtual DbColumn ToInlineTableColumn(DbTable table)
        {
            return null;
        }

        //
        // Equality functions for equality operators
        //

        protected bool Equals(DbColumn other)
        {
            return Equals(DbColumnImpl, other.DbColumnImpl);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DbColumn)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (DbColumnImpl != null ? DbColumnImpl.GetHashCode() : 0) * 397;
            }
        }
    }

    public class DbValueColumn<T> : IDbColumnImpl
    {
        private readonly T _value;

        public Type Type { get; set; }
        public DbTable Table => null;
        public bool IsAggregate => false;
        public bool IsGroupable => false;

        public DbValueColumn(T value)
        {
            _value = value;
            Type = typeof(T);
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            return _value != null ? builderContext.ToSqlValue(_value, typeof(T), usePlaceholder) : "NULL";
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }
    }

    public class DbExprColumn : IDbColumnImpl
    {
        private readonly string _expression;
        private readonly IEnumerable<IDbColumnImpl> _columnList;

        public Type Type { get; set; }
        public DbTable Table => null;
        public bool IsAggregate { get; }
        public bool IsGroupable => !IsAggregate;

        public DbExprColumn(string expression, bool isAggregate, IEnumerable<IDbColumnImpl> columnList)
        {
            _expression = expression;
            _columnList = columnList;
            IsAggregate = isAggregate;
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            if (builderContext.DatabaseProvider == DatabaseProvider.Sqlite && _expression.StartsWith("CONCAT"))
            {
                return string.Join(" || ", _columnList.Select(column => column.Sql(builderContext, usePlaceholder)));
            }
            return string.Format(_expression, _columnList.Select(column => column.Sql(builderContext, usePlaceholder)).Cast<object>().ToArray());
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            // Expressions should always have an alias for sorting
            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }
    }

    public class DbListColumn : IDbColumnImpl
    {
        private readonly IEnumerable<IDbColumnImpl> _columnList;

        public Type Type { get; set; }
        public DbTable Table => null;
        public bool IsAggregate => false;
        public bool IsGroupable => false;

        public DbListColumn(IEnumerable<IDbColumnImpl> columnList)
        {
            _columnList = columnList;
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            return $"({string.Join(", ", _columnList.Select(column => column.Sql(builderContext, false)))})"; // Do not use placeholder in an IN clause
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }
    }

    public class DbTableColumn : IDbColumnImpl
    {
        private readonly int _size;
        private readonly int _scale;

        public Type Type { get; set; }
        public bool IsAggregate => false;
        public bool IsGroupable => true;
        public DbTable Table { get; }

        public string Name { get; }

        public DbTableColumn(DbTable table, string name, int size, int scale)
        {
            _size = size;
            _scale = scale;
            Table = table;
            Name = name;
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            var name = $"{builderContext.IdentifierBeginEscapeCharacter}{Name}{builderContext.IdentifierEndEscapeCharacter}";

            if (Table != null && builderContext.StatementType == StatementType.Select)
            {
                name = $"{builderContext.GetAlias(Table)}.{name}";
            }

            return  name;
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }

        public string Adjust(string value)
        {
            if (value != null)
            {
                value = value.Trim();
                if (_size > 0 && value.Length > _size)
                    value = value.Substring(0, _size);
            }

            return value;
        }
    }

    public class DbInlineTableColumn : IDbColumnImpl
    {
        private readonly IDbColumnImpl _column;

        public Type Type { get => _column?.Type; set { if (_column != null) _column.Type = value; } }
        public DbTable Table { get; }
        public bool IsAggregate => false;
        public bool IsGroupable => !IsAggregate;

        public DbInlineTableColumn(DbTable table, IDbColumnImpl column)
        {
            Table = table;
            _column = column;
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            if (Table != null && _column != null && builderContext.StatementType == StatementType.Select)
            {
                var tableContext = builderContext.ContextForTable(Table);

                if (tableContext != null)
                {
                    return $"{builderContext.GetAlias(Table)}.{tableContext.Alias(_column)}";
                }
            }

            return string.Empty;
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            // Inline columns should always have an alias for sorting
            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }
    }

    public class DbConditionColumn : IDbColumnImpl
    {
        private readonly DbConstraint _constraint;

        public Type Type { get; set; }
        public DbTable Table => null;
        public bool IsAggregate => false;
        public bool IsGroupable => false;

        public DbConditionColumn(DbConstraint constraint)
        {
            _constraint = constraint;
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            return _constraint.Sql(builderContext, usePlaceholder);
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }
    }

    public class DbQueryColumn : IDbColumnImpl
    {
        private readonly ISqlBuilder _query;

        public Type Type { get; set; }
        public DbTable Table => null;
        public bool IsAggregate => false;
        public bool IsGroupable => false;

        public DbQueryColumn(ISqlBuilder query)
        {
            _query = query;
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            return $"({_query.Sql(builderContext, false, out _)})"; // Do not use placeholder in an IN clause
        }

        public string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = Sql(builderContext, usePlaceholder);

            var alias = builderContext.Alias(this);
            if (!string.IsNullOrEmpty(alias))
            {
                result += " " + alias;
            }

            return result;
        }
    }

    //
    // Typed columns
    //

    public class DbTypedColumn<T> : DbColumn
    {
        public DbTypedColumn(DbColumn column) : base(column.DbColumnImpl)
        {
            DbColumnImpl.Type = typeof(T);
        }

        public DbTypedColumn(IDbColumnImpl dbColumnImpl) : base(dbColumnImpl)
        {
            dbColumnImpl.Type = typeof(T);
        }

        public new DbTypedColumn<T> Max()
        {
            return new DbTypedColumn<T>(new DbExprColumn("MAX({0})", true, new[] { this.DbColumnImpl }));
        }

        internal override DbColumn ToInlineTableColumn(DbTable table)
        {
            return new DbTypedColumn<T>(new DbInlineTableColumn(table, DbColumnImpl));
        }
    }

    //
    // Bound value types
    //

    public class DbBoundValue
    {
        public object DbValue { get; set; }

        public bool IsNull => DbValue == null;
        public bool IsNotNull => DbValue != null;

        public void SetValue(DbResultSet resultSet, int index)
        {
            SetValue(resultSet.GetValue(index));
        }

        public void SetValue(object value)
        {
            DbValue = value is DBNull ? null : value;
            OnDbValueChanged();
        }

        protected virtual void OnDbValueChanged() { }
    }

    public class DbTypedBoundValue<T> : DbBoundValue
    {
        static T Convert(object value)
        {
            return (T)DbValueConverter.ChangeType(value, typeof(T));
        }

        public T Value
        {
            get => Convert(DbValue);
            set => DbValue = value;
        }

        public Action<T> Setter { get; set; }

        protected override void OnDbValueChanged()
        {
            Setter?.Invoke(Value);
        }

        public static implicit operator T(DbTypedBoundValue<T> boundValue) => boundValue.Value;

        public override string ToString() => Value.ToString();
    }
}
