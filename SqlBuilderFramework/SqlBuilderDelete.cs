﻿using System.Data;
using System.Text;

namespace SqlBuilderFramework
{
    public class SqlBuilderDelete<T> : SqlBuilderBase<T> where T : DbTable
    {
        public override StatementType StatementType => StatementType.Delete;

        public DbConstraint Constraint { get; private set; }

        public SqlBuilderDelete(T table, IDatabase database = null)
            : base(table, database)
        {
        }

        public SqlBuilderDelete<T> Where(DbConstraint constraint)
        {
            Constraint = constraint;
            return this;
        }

        public SqlBuilderDelete<T> And(DbConstraint constraint)
        {
            if (Constraint == null)
                return Where(constraint);

            Constraint &= constraint;
            return this;
        }

        public SqlBuilderDelete<T> Or(DbConstraint constraint)
        {
            if (Constraint == null)
                return Where(constraint);

            Constraint |= constraint;
            return this;
        }

        public override string CreateSql(BuilderContext builderContext, bool usePlaceholder)
        {
            var sql = new StringBuilder();

            sql.Append("DELETE ");
            if (Constraint == null)
            {
                sql.Append(Table.Definition(builderContext, usePlaceholder));
            }
            else
            {
                sql.Append("FROM ");
                sql.Append(Table.Definition(builderContext, usePlaceholder));
                sql.Append(" WHERE ");
                sql.Append(Constraint.Sql(builderContext, usePlaceholder));
            }

            return sql.ToString();
        }
    }
}
