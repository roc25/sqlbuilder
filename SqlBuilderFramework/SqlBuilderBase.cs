﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace SqlBuilderFramework
{
    public abstract class SqlBuilderBase<TT> : ISqlBuilder where TT : DbTable
    {
        #region DbMapper factory

        public static DbMapper<C> Mapper<C>(Func<C> addEntity) where C : new()
        {
            return new DbMapper<C>(addEntity);
        }

        #endregion

        #region Data source

        protected readonly TT Table;

        #endregion

        #region Output columns

        protected readonly List<OutputColumn> ColumnBindings = new List<OutputColumn>();

        protected IDbColumnImpl[] OutputColumns => ColumnBindings.Select(o => o.Column).ToArray();

        #endregion

        #region Public properties

        public abstract StatementType StatementType { get; }

        public IDatabase Database { get; protected set; }

        public bool HasOutput => ColumnBindings.Any();

        #endregion

        #region Constructor

        protected SqlBuilderBase(TT table, IDatabase database = null)
        {
            Table = table;
            Database = database;
        }

        #endregion

        #region Reader

        public DbBoundValue Bind(DbColumn column)
        {
            var boundValue = new DbBoundValue();
            ColumnBindings.Add(new OutputColumn { Column = column.DbColumnImpl, BoundValue = boundValue });
            return boundValue;
        }

        public DbTypedBoundValue<T> Bind<T>(DbTypedColumn<T> column)
        {
            var boundValue = new DbTypedBoundValue<T>();
            ColumnBindings.Add(new OutputColumn { Column = column.DbColumnImpl, BoundValue = boundValue });
            return boundValue;
        }

        public T ReadValue<T>(IDatabase database, DbTypedColumn<T> column, T defaultValue = default)
        {
            Database = database;
            return ReadValue(column);
        }

        public T ReadValue<T>(DbTypedColumn<T> column, T defaultValue = default)
        {
            ColumnBindings.Clear();

            var col = Bind(column);

            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    return col.Value;
                }
            }

            return defaultValue;
        }

        public Tuple<T1, T2> ReadValue<T1, T2>(IDatabase database, DbTypedColumn<T1> column1, DbTypedColumn<T2> column2)
        {
            Database = database;
            return ReadValue(column1, column2);
        }

        public Tuple<T1, T2> ReadValue<T1, T2>(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2)
        {
            ColumnBindings.Clear();

            var col1 = Bind(column1);
            var col2 = Bind(column2);

            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    return new Tuple<T1, T2>(col1.Value, col2.Value);
                }
            }

            return null;
        }

        public Tuple<T1, T2, T3> ReadValue<T1, T2, T3>(IDatabase database, DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, DbTypedColumn<T3> column3)
        {
            Database = database;
            return ReadValue(column1, column2, column3);
        }

        public Tuple<T1, T2, T3> ReadValue<T1, T2, T3>(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, DbTypedColumn<T3> column3)
        {
            ColumnBindings.Clear();

            var col1 = Bind(column1);
            var col2 = Bind(column2);
            var col3 = Bind(column3);

            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    return new Tuple<T1, T2, T3>(col1.Value, col2.Value, col3.Value);
                }
            }

            return null;
        }

        public List<T> ReadValues<T>(IDatabase database, DbTypedColumn<T> column)
        {
            Database = database;
            return ReadValues(column);
        }

        public List<T> ReadValues<T>(DbTypedColumn<T> column)
        {
            ColumnBindings.Clear();

            var result = new List<T>();
            var col = Bind(column);

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    result.Add(col.Value);
                }
            }

            return result;
        }

        public bool ReadFirst(IDatabase database)
        {
            Database = database;
            return ReadFirst();
        }

        public bool ReadFirst()
        {
            using (var reader = ExecuteReader())
            {
                return reader.Next();
            }
        }

        public bool ReadFirst(IDatabase database, Action action)
        {
            Database = database;
            return ReadFirst(action);
        }

        public bool ReadFirst(Action action)
        {
            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    action();
                    return true;
                }
            }

            return false;
        }

        public bool ReadFirst<T>(IDatabase database, DbTypedColumn<T> column, Action<T> action)
        {
            Database = database;
            return ReadFirst(column, action);
        }

        public bool ReadFirst<T>(DbTypedColumn<T> column, Action<T> action)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(column);
            var recordRead = false;

            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    action(b1.Value);
                    recordRead = true;
                }
            }

            ColumnBindings.RemoveRange(selCount, 1);

            return recordRead;
        }

        public bool ReadFirst<T1, T2>(IDatabase database, DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, Action<T1, T2> action)
        {
            Database = database;
            return ReadFirst(col1, col2, action);
        }

        public bool ReadFirst<T1, T2>(DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, Action<T1, T2> action)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(col1);
            var b2 = Bind(col2);
            var recordRead = false;

            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    action(b1.Value, b2.Value);
                    recordRead = true;
                }
            }

            ColumnBindings.RemoveRange(selCount, 2);

            return recordRead;
        }

        public bool ReadFirst<T1, T2, T3>(IDatabase database, DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, DbTypedColumn<T3> col3, Action<T1, T2, T3> action)
        {
            Database = database;
            return ReadFirst(col1, col2, col3, action);
        }

        public bool ReadFirst<T1, T2, T3>(DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, DbTypedColumn<T3> col3, Action<T1, T2, T3> action)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(col1);
            var b2 = Bind(col2);
            var b3 = Bind(col3);
            var recordRead = false;

            using (var reader = ExecuteReader())
            {
                if (reader.Next())
                {
                    action(b1.Value, b2.Value, b3.Value);
                    recordRead = true;
                }
            }

            ColumnBindings.RemoveRange(selCount, 3);

            return recordRead;
        }

        public List<TC> ReadAll<TC>(IDatabase database, DbMapper<TC> mapper) where TC : new()
        {
            Database = database;
            return ReadAll(mapper);
        }

        public List<TC> ReadAll<TC>(DbMapper<TC> mapper) where TC : new()
        {
            var selCount = ColumnBindings.Count;

            mapper.BindColumns(this);

            using (var reader = ExecuteReader())
            {
                mapper.NewList();

                while (reader.Next())
                {
                    mapper.AddEntity();
                }

                mapper.End();
            }

            ColumnBindings.RemoveRange(selCount, ColumnBindings.Count - selCount);

            return mapper.EntityList;
        }

        public List<TC> ReadAll<TC>(IDatabase database, Func<TT, DbMapper<TC>> getMapper) where TC : new()
        {
            Database = database;
            return ReadAll(getMapper);
        }

        public List<TC> ReadAll<TC>(Func<TT, DbMapper<TC>> getMapper) where TC : new()
        {
            return ReadAll(getMapper(Table));
        }

        public List<Tuple<TC1, TC2>> ReadAll<TC1, TC2>(IDatabase database, DbMapper<TC1> mapper1, DbMapper<TC2> mapper2) where TC1 : new() where TC2 : new()
        {
            Database = database;
            return ReadAll(mapper1, mapper2);
        }

        public List<Tuple<TC1, TC2>> ReadAll<TC1, TC2>(DbMapper<TC1> mapper1, DbMapper<TC2> mapper2) where TC1 : new() where TC2 : new()
        {
            var selCount = ColumnBindings.Count;

            mapper1.BindColumns(this);
            mapper2.BindColumns(this);

            using (var reader = ExecuteReader())
            {
                mapper1.NewList();
                mapper2.NewList();

                while (reader.Next())
                {
                    mapper1.AddEntity();
                    mapper2.AddEntity();
                }

                mapper1.End();
                mapper2.End();
            }

            ColumnBindings.RemoveRange(selCount, ColumnBindings.Count - selCount);

            return mapper1.EntityList.Zip(mapper2.EntityList, (c1, c2) => new Tuple<TC1, TC2>(c1, c2)).ToList();
        }

        public void ReadAll(IDatabase database, Action action)
        {
            Database = database;
            ReadAll(action);
        }

        public void ReadAll(Action action)
        {
            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    action();
                }
            }
        }

        public void ReadAll<T>(IDatabase database, DbTypedColumn<T> column, Action<T> action)
        {
            Database = database;
            ReadAll(column, action);
        }

        public void ReadAll<T>(DbTypedColumn<T> column, Action<T> action)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(column);

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    action(b1.Value);
                }
            }

            ColumnBindings.RemoveRange(selCount, 1);
        }

        public IEnumerable<TR> ReadAll<T, TR>(IDatabase database, DbTypedColumn<T> column, Func<T, TR> func)
        {
            Database = database;
            return ReadAll(column, func);
        }

        public IEnumerable<TR> ReadAll<T, TR>(DbTypedColumn<T> column, Func<T, TR> func)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(column);

            var result = new List<TR>();

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    result.Add(func(b1.Value));
                }
            }

            ColumnBindings.RemoveRange(selCount, 1);

            return result;
        }

        public void ReadAll<T1, T2>(IDatabase database, DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, Action<T1, T2> action)
        {
            Database = database;
            ReadAll(col1, col2, action);
        }

        public void ReadAll<T1, T2>(DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, Action<T1, T2> action)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(col1);
            var b2 = Bind(col2);

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    action(b1.Value, b2.Value);
                }
            }

            ColumnBindings.RemoveRange(selCount, 2);
        }

        public IEnumerable<TR> ReadAll<T1, T2, TR>(IDatabase database, DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, Func<T1, T2, TR> func)
        {
            Database = database;
            return ReadAll(col1, col2, func);
        }

        public IEnumerable<TR> ReadAll<T1, T2, TR>(DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, Func<T1, T2, TR> func)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(col1);
            var b2 = Bind(col2);

            var result = new List<TR>();

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    result.Add(func(b1.Value, b2.Value));
                }
            }

            ColumnBindings.RemoveRange(selCount, 2);

            return result;
        }

        public void ReadAll<T1, T2, T3>(IDatabase database, DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, DbTypedColumn<T3> col3, Action<T1, T2, T3> action)
        {
            Database = database;
            ReadAll(col1, col2, col3, action);
        }

        public void ReadAll<T1, T2, T3>(DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, DbTypedColumn<T3> col3, Action<T1, T2, T3> action)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(col1);
            var b2 = Bind(col2);
            var b3 = Bind(col3);

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    action(b1.Value, b2.Value, b3.Value);
                }

                ColumnBindings.RemoveRange(selCount, 3);
            }
        }

        public IEnumerable<TR> ReadAll<T1, T2, T3, TR>(IDatabase database, DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, DbTypedColumn<T3> col3, Func<T1, T2, T3, TR> func)
        {
            Database = database;
            return ReadAll(col1, col2, col3, func);
        }

        public IEnumerable<TR> ReadAll<T1, T2, T3, TR>(DbTypedColumn<T1> col1, DbTypedColumn<T2> col2, DbTypedColumn<T3> col3, Func<T1, T2, T3, TR> func)
        {
            var selCount = ColumnBindings.Count;

            var b1 = Bind(col1);
            var b2 = Bind(col2);
            var b3 = Bind(col3);

            var result = new List<TR>();

            using (var reader = ExecuteReader())
            {
                while (reader.Next())
                {
                    result.Add(func(b1.Value, b2.Value, b3.Value));
                }
            }

            ColumnBindings.RemoveRange(selCount, 3);

            return result;
        }

        #endregion

        #region Executors

        public ISqlBuilderReader ExecuteReader(IDatabase database)
        {
            Database = database;
            return ExecuteReader();
        }

        public virtual ISqlBuilderReader ExecuteReader()
        {
            using (var command = Command())
            {
                return new SqlBuilderReader(command?.ExecuteReader(), this);
            }
        }

        protected ISqlBuilderReader ExecuteOracleNonQueryReader()
        {
            // DML ausfühern
            using (var command = Command())
            {
                var affectedRows = command.ExecuteNonQuery();

                if (affectedRows == 1 && HasOutput) // Get implicit return values
                {
                    SetValues(command.Parameters.Select(p => p.Value));

                    return new SqlSimReader();
                }
            }

            return null;
        }

        public int Execute(IDatabase database)
        {
            return ExecuteNonQuery(database);
        }

        public int Execute()
        {
            return ExecuteNonQuery();
        }

        public int ExecuteNonQuery(IDatabase database)
        {
            Database = database;
            return ExecuteNonQuery();
        }

        public virtual int ExecuteNonQuery()
        {
            if (HasOutput)
            {
                using (var reader = ExecuteReader())
                {
                    if (reader != null)
                    {
                        return reader.Next() ? 1 : 0;
                    }
                }

                return 0;
            }

            using (var command = Command())
            {
                if (command == null)
                    return 0;

                // DML ausfühern
                return command.ExecuteNonQuery();
            }
        }

        protected ISqlStatement Command()
        {
            if (Database == null)
                return null;

            var parameterList = new List<ParameterEntity>();
            var command = Database.CreateStatement(Sql(parameterList, Database));

            if (string.IsNullOrEmpty(command.Sql))
                return null;

            if (StatementType != StatementType.Select)
            {
                if (Database.Provider == DatabaseProvider.Oracle)
                {
                    // Supply the parameters for the RETURNING...INTO clause
                    var counter = 0;

                    foreach (var column in ColumnBindings)
                    {
                        command.AddParameter("par" + counter.ToString("D2"), null, Database.ToDbType(column.Column.Type), 0, ParameterDirection.Output);
                        counter++;
                    }
                }
            }

            foreach (var parameter in parameterList)
            {
                command.AddParameter(parameter.Name, parameter.Value, Database.ToDbType(parameter.Type));
            }

            return command;
        }

        public void SetValues(DbResultSet resultSet)
        {
            for (int i = 0; i < ColumnBindings.Count; ++i)
                ColumnBindings[i].BoundValue?.SetValue(resultSet, i);
        }

        protected void SetValues(IEnumerable<object> values)
        {
            int i = 0;

            foreach (var value in values)
            {
                if (i == ColumnBindings.Count)
                    break;

                ColumnBindings[i].BoundValue?.SetValue(value);
                i++;
            }
        }

        #endregion

        #region SQL builder

        public string Sql(List<ParameterEntity> parameterList, IDatabase database = null)
        {
            return CreateSql(new BuilderContext(database ?? Database, StatementType, OutputColumns, parameterList), parameterList != null);
        }

        public string Sql(BuilderContext parentContext, bool usePlaceholder, out BuilderContext builderContext)
        {
            builderContext = new BuilderContext(parentContext, StatementType, OutputColumns);
            return CreateSql(builderContext, usePlaceholder);
        }

        public abstract string CreateSql(BuilderContext builderContext, bool usePlaceholder);

        #endregion

        #region Internal classes

        protected class OutputColumn
        {
            public IDbColumnImpl Column { get; set; }

            public DbBoundValue BoundValue { get; set; }
        }

        protected class Assignments
        {
            private int _assignmentRow;

            public readonly List<ColumnAssignments> AssignmentColumns = new List<ColumnAssignments>();

            public bool HasAssignments => AssignmentColumns.Any();

            public void Add(IEnumerable<DbAssignment> assignments)
            {
                foreach (var assignment in assignments ?? Enumerable.Empty<DbAssignment>())
                {
                    var columnAssignments = AssignmentColumns.FirstOrDefault(c => assignment.Left.DbColumnImpl == c.Column);

                    if (columnAssignments == null)
                    {
                        columnAssignments = new ColumnAssignments { Column = assignment.Left.DbColumnImpl, AssignedColumns = new List<IDbColumnImpl>() };
                        AssignmentColumns.Add(columnAssignments);
                    }

                    while (columnAssignments.AssignedColumns.Count <= _assignmentRow)
                    {
                        columnAssignments.AssignedColumns.Add(null);
                    }

                    columnAssignments.AssignedColumns[_assignmentRow] = assignment.Right.DbColumnImpl;
                }
            }

            public void AddAssignmentsFromEntity<TE>(DbTable table, TE newEntity, TE currentEntity) where TE : class
            {
                var entityType = typeof(TE);

                // Add assignments from fields with DbColumnAttribute
                foreach (var fieldInfo in entityType.GetFields())
                {
                    var annotation = fieldInfo.GetCustomAttribute(typeof(DbColumnAttribute)) as DbColumnAttribute;

                    if (annotation == null || annotation.ReadOnly)
                    {
                        continue;
                    }

                    var column = annotation.GetColumnFromAnnotation(table, fieldInfo.Name);

                    if (column is object)
                    {
                        var newValue = fieldInfo.GetValue(newEntity);
                        var currentValue = currentEntity != null ? fieldInfo.GetValue(currentEntity) : null;

                        if (currentEntity == null || !object.Equals(newValue, currentValue))
                        {
                            Add(new[] { new DbAssignment(column, new DbColumn(new DbValueColumn<object>(newValue))) });
                        }
                    }
                }

                // Add assignments from properties with DbColumnAttribute
                foreach (var propertyInfo in entityType.GetProperties())
                {
                    var annotation = propertyInfo.GetCustomAttribute(typeof(DbColumnAttribute)) as DbColumnAttribute;

                    if (annotation == null || annotation.ReadOnly)
                    {
                        continue;
                    }

                    var column = annotation.GetColumnFromAnnotation(table, propertyInfo.Name);

                    if (column is object)
                    {
                        var newValue = propertyInfo.GetValue(newEntity);
                        var currentValue = currentEntity != null ? propertyInfo.GetValue(currentEntity) : null;

                        if (currentEntity == null || !object.Equals(newValue, currentValue))
                        {
                            Add(new[] { new DbAssignment(column, new DbColumn(new DbValueColumn<object>(newValue))) });
                        }
                    }
                }

                // Add assignments from methods returning IEnumerable<DbAssignment>
                foreach (var methodInfo in entityType.GetMethods(BindingFlags.Public))
                {
                    var returnType = methodInfo.ReturnType;

                    if (!typeof(IEnumerable<DbAssignment>).IsAssignableFrom(returnType))
                    {
                        continue;
                    }

                    var parameterInfos = methodInfo.GetParameters();
                    var parameters = new List<object>();

                    if (parameterInfos.Length != 1 && parameterInfos.Length != 2)
                    {
                        continue;
                    }

                    var tableParameterType = parameterInfos[0].ParameterType;

                    if (!typeof(DbTable).IsAssignableFrom(tableParameterType))
                    {
                        continue;
                    }

                    var tableSource = table.Get(tableParameterType);

                    if (tableSource == null)
                    {
                        continue;
                    }

                    parameters.Add(tableSource);

                    if (parameterInfos.Length == 2)
                    {
                        var currentEntityParameterType = parameterInfos[1].ParameterType;

                        if (!currentEntityParameterType.IsAssignableFrom(entityType))
                        {
                            continue;
                        }

                        parameters.Add(currentEntity);
                    }

                    var assignments = (IEnumerable<DbAssignment>)methodInfo.Invoke(newEntity, parameters.ToArray());

                    Add(assignments);
                }
            }

            public void StashRow()
            {
                if (_assignmentRow < AssignmentColumns.Max(assignments => assignments.AssignedColumns.Count))
                {
                    _assignmentRow++;
                }
            }

            public void ClearStash()
            {
                _assignmentRow = 0;
            }

            public class ColumnAssignments
            {
                public IDbColumnImpl Column { get; set; }

                public List<IDbColumnImpl> AssignedColumns { get; set; } // Multiple columns for bulk insert
            }
        }

        #endregion
    }
}
