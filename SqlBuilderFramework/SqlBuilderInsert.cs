﻿using System;
using System.Data;
using System.Linq;
using System.Text;

namespace SqlBuilderFramework
{
    public class SqlBuilderInsert<T> : SqlBuilderBase<T> where T : DbTable
    {
        private readonly Assignments _assignments = new Assignments();
        private ISqlBuilder _from;

        public override StatementType StatementType => StatementType.Insert;

        public SqlBuilderInsert(T table, IDatabase database = null)
            : base(table, database)
        {
        }

        public SqlBuilderInsert<T> Set(params DbAssignment[] assignments)
        {
            _assignments.Add(assignments);

            return this;
        }

        public SqlBuilderInsert<T> SetEntity<TE>(TE entity) where TE : class
        {
            _assignments.AddAssignmentsFromEntity(Table, entity, null);

            return this;
        }

        public SqlBuilderInsert<T> Set(IDbInsertAssignments<T> insertAssignments)
        {
            _assignments.Add(insertAssignments.GetAssignments(Table).ToArray());
            return this;
        }

        public SqlBuilderInsert<T> From(ISqlBuilder query)
        {
            _from = query;
            return this;
        }

        public void StashRow()
        {
            _assignments.StashRow();
        }

        public void ClearStash()
        {
            _assignments.ClearStash();
        }

        public SqlBuilderInsert<T> Map<TC>(DbTypedColumn<TC> column, Action<TC> action)
        {
            Bind(column).Setter = action;
            return this;
        }

        public override ISqlBuilderReader ExecuteReader()
        {
            if (Database.Provider == DatabaseProvider.Sqlite)
            {
                using (var command = Command())
                using (var reader = command.ExecuteReader()) // SQLite kann nur die zuletzt eingefügte RowId zurückgeben (siehe auch die Methode sql())
                {
                    if (reader.Next())
                    {
                        var rowId = reader.GetLong(0);

                        if (rowId > 0L)
                        {
                            var builderContext = new BuilderContext(Database, StatementType.Select, OutputColumns);
                            var fromClause = Table.Definition(builderContext, false); // Must be defined before the column definitions to have the table aliases
                            return new SqlBuilderReader(
                                Database.ExecuteReader(
                                    $"SELECT {string.Join(", ", OutputColumns.Select(column => column.Definition(builderContext, false)))} FROM {fromClause} WHERE rowid = {rowId}"
                                ),
                                this);
                        }
                    }
                }

                return null;
            }

            if (Database.Provider == DatabaseProvider.Oracle)
            {
                return ExecuteOracleNonQueryReader();
            }

            return base.ExecuteReader();
        }

        public override int ExecuteNonQuery()
        {
            _assignments.ClearStash();

            return base.ExecuteNonQuery();
        }

        public override string CreateSql(BuilderContext builderContext, bool usePlaceholder)
        {
            var sql = new StringBuilder();

            sql.Append("INSERT INTO ");
            sql.Append(Table.Definition(builderContext, usePlaceholder));
            sql.Append(" (");
            sql.Append(string.Join(", ", _assignments.AssignmentColumns.Select(columnAssignments => columnAssignments.Column.Sql(builderContext, usePlaceholder))));
            sql.Append(")");
            if (HasOutput) // Nach dem Insert sollen Werte zurückgegeben werden
            {
                if (Database.Provider == DatabaseProvider.MsSql)
                {
                    sql.Append(" OUTPUT ");
                    sql.Append(string.Join(", ", builderContext.OutputColumns.Select(column => "INSERTED.[" + column.Sql(builderContext, false) + "]")));
                    //sql.Append(" INTO ");
                    //for (int i = 0; i < ColumnBindings.Count; i++)
                    //{
                    //    if (i > 0)
                    //        sql.Append(", ");
                    //    sql.Append("@par" + i.ToString("D2"));
                    //}
                }
            }

            if (_from == null)
            {
                sql.Append(" VALUES (");
                sql.Append(string.Join(", ", _assignments.AssignmentColumns.Select(columnAssignments => "(" + string.Join(", ", columnAssignments.AssignedColumns.Select(value => value.Sql(builderContext, usePlaceholder))) + ")")));
                sql.Append(")");
                if (HasOutput) // Nach dem Insert sollen Werte zurückgegeben werden
                {
                    if (Database.Provider == DatabaseProvider.Oracle)
                    {
                        var outputColumns = builderContext.OutputColumns;

                        // Bei Oracle geschieht dies mit der RETURNING ... INTO clause.
                        sql.Append(" RETURNING ");
                        sql.Append(string.Join(", ", outputColumns.Select(column => column.Sql(builderContext, false))));
                        sql.Append(" INTO ");
                        for (int i = 0; i < outputColumns.Length; i++)
                        {
                            if (i > 0)
                                sql.Append(", ");
                            sql.Append(":par" + i.ToString("D2"));
                        }
                    }
                    else if (Database.Provider == DatabaseProvider.Sqlite)
                    {
                        // Bei SQLite bekommt man nur den letzten Auto-Increment-Wert zurück
                        // Achtung: last_insert_rowid() funktioniert nur auf der gleichen Connection, d.h. die Connection darf zwischenzeitlich nicht geschlossen worden sein.
                        //          Deshalb werden die beiden Kommandos hier in einem Befehl zusammengefaßt.
                        sql.Append("; SELECT last_insert_rowid();");
                    }
                }
            }
            else
            {
                sql.Append(" ");
                sql.Append(_from.CreateSql(
                    new BuilderContext(builderContext, StatementType.Select, _assignments.AssignmentColumns.Select(a => a.AssignedColumns[0]).ToArray()),
                    usePlaceholder));
            }

            return sql.ToString();
        }
    }
}
