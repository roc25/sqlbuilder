﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SqlBuilderFramework
{
    public class BuilderContext
    {
        private int _subCommandCount;
        private readonly string _tablePrefix;
        private readonly BuilderContext _parentContext;
        private readonly List<Tuple<DbTable, BuilderContext>> _queryTables = new List<Tuple<DbTable, BuilderContext>>();
        private readonly List<ParameterEntity> _parameterList = new List<ParameterEntity>();
        private readonly IDatabase _database;

        public readonly StatementType StatementType;

        public readonly DatabaseProvider DatabaseProvider;
        public readonly bool SchemaSupported;
        public readonly char IdentifierBeginEscapeCharacter;
        public readonly char IdentifierEndEscapeCharacter;
        public readonly IDbColumnImpl[] OutputColumns;

        public BuilderContext(BuilderContext parentContext, StatementType statementType, IDbColumnImpl[] outputColumns)
            : this(parentContext._database, statementType, outputColumns, parentContext._parameterList)
        {
            parentContext._subCommandCount++;
            _tablePrefix = $"{parentContext._tablePrefix}S{parentContext._subCommandCount}";
            _parentContext = parentContext;
        }

        public BuilderContext(IDatabase database, StatementType statementType, IDbColumnImpl[] outputColumns, List<ParameterEntity> parameterList = null)
        {
            OutputColumns = outputColumns;
            if (parameterList != null)
            {
                _parameterList = parameterList;
            }
            _database = database;

            StatementType = statementType;

            DatabaseProvider = database.Provider;
            switch (DatabaseProvider)
            {
                case DatabaseProvider.Oracle:
                    SchemaSupported = true;
                    IdentifierBeginEscapeCharacter = '"';
                    IdentifierEndEscapeCharacter = '"';
                    break;
                case DatabaseProvider.Sqlite:
                    SchemaSupported = false;
                    IdentifierBeginEscapeCharacter = '"';
                    IdentifierEndEscapeCharacter = '"';
                    break;
                case DatabaseProvider.MsSql:
                    SchemaSupported = true;
                    IdentifierBeginEscapeCharacter = '[';
                    IdentifierEndEscapeCharacter = ']';
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public string DefineAlias(DbTable table, BuilderContext builderContext)
        {
            var index = _queryTables.FindIndex(tbl => tbl.Item1 == table);

            if (index == -1)
            {
                index = _queryTables.Count;
                _queryTables.Add(new Tuple<DbTable, BuilderContext>(table, builderContext));
            }

            return $"{_tablePrefix}T{index + 1}";
        }

        public string GetAlias(DbTable table)
        {
            var context = this;

            while (context != null)
            {
                var index = context._queryTables.FindIndex(tbl => tbl.Item1 == table);

                if (index >= 0)
                {
                    return $"{context._tablePrefix}T{index + 1}";
                }

                context = context._parentContext;
            }

            return null;
        }

        public BuilderContext ContextForTable(DbTable table)
        {
            var context = this;

            while (context != null)
            {
                var tableContext = context._queryTables.FirstOrDefault(tbl => tbl.Item1 == table)?.Item2;

                if (tableContext != null)
                {
                    return tableContext;
                }

                context = context._parentContext;
            }

            return null;
        }

        public string Alias(IDbColumnImpl column)
        {
            var index = Array.IndexOf(OutputColumns, column);

            if (index == -1)
            {
                return null;
            }

            return $"COL{index + 1}";
        }

        public string ToSqlValue(object value, Type type, bool useParameter)
        {
            if (!useParameter || value == null || value is bool || value is int || value is long)
                return _database.ToLiteral(value);

            var parId = "ph" + _parameterList.Count.ToString("D2");

            _parameterList.Add(new ParameterEntity(parId, type, value));

            return _database.ToParameterName(parId);
        }
    }
}