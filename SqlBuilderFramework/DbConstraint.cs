﻿using System.Text;

namespace SqlBuilderFramework
{
    public class DbConstraint
    {
        public DbConstraint()
        {
        }

        public DbConstraint(DbColumn left, string op, DbColumn right)
        {
            _leftColumn = left?.DbColumnImpl;
            _operator = op;
            _rightColumn = right?.DbColumnImpl;
        }

        public static DbConstraint operator &(DbConstraint left, DbConstraint right)
        {
            if (left?._operator == null)
                return right;
            if (right?._operator == null)
                return left;
            return new DbConstraint { _operator = "AND", _leftConstraint = left, _rightConstraint = right };
        }

        public static DbConstraint operator |(DbConstraint left, DbConstraint right)
        {
            if (left?._operator == null)
                return right;
            if (right?._operator == null)
                return left;
            return new DbConstraint { _operator = "OR", _leftConstraint = left, _rightConstraint = right };
        }

        public static DbConstraint operator !(DbConstraint constraint)
        {
            return new DbConstraint { _operator = "NOT", _rightConstraint = constraint };
        }

        public string Sql(BuilderContext builderContext, bool usePlaceholder)
        {
            var sql = new StringBuilder();

            sql.Append("(");

            if (_leftColumn == null && _rightColumn == null)
            {
                if (_leftConstraint != null)
                {
                    sql.Append(_leftConstraint.Sql(builderContext, usePlaceholder));
                    sql.Append(" ");
                }
                sql.Append(_operator);
                if (_rightConstraint != null)
                {
                    sql.Append(" ");
                    sql.Append(_rightConstraint.Sql(builderContext, usePlaceholder));
                }
            }
            else
            {
                string lValue = null;
                string rValue = null;
                string op = _operator;

                if (_leftColumn != null)
                    lValue = _leftColumn.Sql(builderContext, usePlaceholder);
                if (_rightColumn != null)
                {
                    rValue = _rightColumn.Sql(builderContext, usePlaceholder);
                    if (rValue == "NULL")
                        op = (op == "=" ? "IS" : "IS NOT");
                }

                if (!string.IsNullOrEmpty(lValue))
                {
                    sql.Append(lValue);
                    sql.Append(" ");
                }

                sql.Append(op);

                if (!string.IsNullOrEmpty(rValue))
                {
                    sql.Append(" ");
                    sql.Append(rValue);
                }
            }

            sql.Append(")");

            return sql.ToString();
        }

        private string _operator;
        private readonly IDbColumnImpl _leftColumn;
        private readonly IDbColumnImpl _rightColumn;
        private DbConstraint _leftConstraint;
        private DbConstraint _rightConstraint;
    }
}
