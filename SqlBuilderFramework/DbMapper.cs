﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SqlBuilderFramework
{
    public interface IDbMapperEntry<C>
    {
        void BindColumns(ISqlBuilder sqlBuilder);
        bool IsNull { get; }
        void SetValues(C entity);
    }

    public class DbMapperEntry<C> : IDbMapperEntry<C>
    {
        private readonly DbColumn column;
        private DbBoundValue boundValue;
        private readonly Action<C, object> setter;

        public DbMapperEntry(DbColumn column, Action<C, object> setter)
        {
            this.column = column;
            this.setter = setter;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            boundValue = sqlBuilder.Bind(column);
        }

        public bool IsNull => boundValue.IsNull;

        public void SetValues(C entity)
        {
            setter(entity, boundValue.DbValue);
        }
    }

    public class DbMapperEntry<C, T> : IDbMapperEntry<C>
    {
        private readonly DbTypedColumn<T> column;
        private DbTypedBoundValue<T> boundValue;
        private readonly Action<C, T> setter;

        public DbMapperEntry(DbTypedColumn<T> column, Action<C, T> setter)
        {
            this.column = column;
            this.setter = setter;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            boundValue = sqlBuilder.Bind(column);
        }

        public bool IsNull => boundValue.IsNull;

        public void SetValues(C entity)
        {
            setter(entity, boundValue.Value);
        }
    }

    public class DbMapperEntry<C, T1, T2> : IDbMapperEntry<C>
    {
        private readonly DbTypedColumn<T1> column1;
        private DbTypedBoundValue<T1> boundValue1;
        private readonly DbTypedColumn<T2> column2;
        private DbTypedBoundValue<T2> boundValue2;
        private readonly Action<C, T1, T2> setter;

        public DbMapperEntry(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, Action<C, T1, T2> setter)
        {
            this.column1 = column1;
            this.column2 = column2;
            this.setter = setter;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            boundValue1 = sqlBuilder.Bind(column1);
            boundValue2 = sqlBuilder.Bind(column2);
        }

        public bool IsNull => boundValue1.IsNull && boundValue2.IsNull;

        public void SetValues(C entity)
        {
            setter(entity, boundValue1.Value, boundValue2.Value);
        }
    }

    public class DbMapperEntry<C, T1, T2, T3> : IDbMapperEntry<C>
    {
        private readonly DbTypedColumn<T1> column1;
        private DbTypedBoundValue<T1> boundValue1;
        private readonly DbTypedColumn<T2> column2;
        private DbTypedBoundValue<T2> boundValue2;
        private readonly DbTypedColumn<T3> column3;
        private DbTypedBoundValue<T3> boundValue3;
        private readonly Action<C, T1, T2, T3> setter;

        public DbMapperEntry(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, DbTypedColumn<T3> column3, Action<C, T1, T2, T3> setter)
        {
            this.column1 = column1;
            this.column2 = column2;
            this.column3 = column3;
            this.setter = setter;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            boundValue1 = sqlBuilder.Bind(column1);
            boundValue2 = sqlBuilder.Bind(column2);
            boundValue3 = sqlBuilder.Bind(column3);
        }

        public bool IsNull => boundValue1.IsNull && boundValue2.IsNull && boundValue3.IsNull;

        public void SetValues(C entity)
        {
            setter(entity, boundValue1.Value, boundValue2.Value, boundValue3.Value);
        }
    }

    public class DbMapperEntry<C, T1, T2, T3, T4> : IDbMapperEntry<C>
    {
        private readonly DbTypedColumn<T1> column1;
        private DbTypedBoundValue<T1> boundValue1;
        private readonly DbTypedColumn<T2> column2;
        private DbTypedBoundValue<T2> boundValue2;
        private readonly DbTypedColumn<T3> column3;
        private DbTypedBoundValue<T3> boundValue3;
        private readonly DbTypedColumn<T4> column4;
        private DbTypedBoundValue<T4> boundValue4;
        private readonly Action<C, T1, T2, T3, T4> setter;

        public DbMapperEntry(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, DbTypedColumn<T3> column3, DbTypedColumn<T4> column4, Action<C, T1, T2, T3, T4> setter)
        {
            this.column1 = column1;
            this.column2 = column2;
            this.column3 = column3;
            this.column4 = column4;
            this.setter = setter;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            boundValue1 = sqlBuilder.Bind(column1);
            boundValue2 = sqlBuilder.Bind(column2);
            boundValue3 = sqlBuilder.Bind(column3);
            boundValue4 = sqlBuilder.Bind(column4);
        }

        public bool IsNull => boundValue1.IsNull && boundValue2.IsNull && boundValue3.IsNull && boundValue4.IsNull;

        public void SetValues(C entity)
        {
            setter(entity, boundValue1.Value, boundValue2.Value, boundValue3.Value, boundValue4.Value);
        }
    }

    public interface IGroupMapper<C>
    {
        void BindColumns(ISqlBuilder sqlBuilder);

        void NewList();

        void SetList(C entity);

        void AddEntity();
    }

    public class GroupMapper<C, E> : IGroupMapper<C>, IDbMapperEntry<C> where E : new()
    {
        private readonly DbMapper<E> mapper;
        private readonly Action<C, List<E>> setter;

        public GroupMapper(DbMapper<E> mapper, Action<C, List<E>> setter)
        {
            this.mapper = mapper;
            this.setter = setter;
        }
        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            mapper.BindColumns(sqlBuilder);
        }

        public bool IsNull => mapper.IsNull;

        public void SetValues(C entity)
        {
            if (mapper.IsNull)
            {
                return;
            }

            mapper.NewList();
            mapper.AddEntity();
            mapper.End();
            setter(entity, mapper.EntityList);
        }

        public void NewList()
        {
            mapper.NewList();
        }

        public void SetList(C entity)
        {
            setter(entity, mapper.EntityList);
        }

        public void AddEntity()
        {
            mapper.AddEntity();
        }
    }

    public interface IGroupPredicate
    {
        void BindColumns(ISqlBuilder sqlBuilder);

        bool GroupChanged();
    }

    public class GroupPredicate<T> : IGroupPredicate
    {
        private readonly DbTypedColumn<T> column;
        private DbTypedBoundValue<T> boundValue;
        private T value;
        private readonly Func<T, T, bool> predicate;

        public GroupPredicate(DbTypedColumn<T> column, Func<T, T, bool> predicate)
        {
            this.column = column;
            this.predicate = predicate;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            boundValue = sqlBuilder.Bind(column);
        }

        public bool GroupChanged()
        {
            var result = predicate(value, boundValue.Value);

            value = boundValue.Value;

            return result;
        }
    }

    public class DbMapper<TC> where TC : new()
    {
        private readonly Func<TC> _newEntity;

        private readonly List<IDbMapperEntry<TC>> _entries = new List<IDbMapperEntry<TC>>();

        private IGroupMapper<TC> _groupMapper;

        private IGroupPredicate _explicitGroupPredicate;

        private Func<TC, TC, bool> _groupPredicate;

        private readonly List<Action<TC>> _instanceInitializingActions = new List<Action<TC>>();

        private readonly List<Action<TC>> _instanceFinalizingActions = new List<Action<TC>>();

        public List<TC> EntityList { get; private set; }

        public DbMapper()
        {
        }

        public DbMapper(Func<TC> newEntity)
        {
            this._newEntity = newEntity;
        }

        public DbMapper<TC> Map<T>(DbColumn column, Action<TC, object> setter)
        {
            _entries.Add(new DbMapperEntry<TC>(column, setter));
            return this;
        }

        public DbMapper<TC> Map<T>(DbTypedColumn<T> column, Action<TC, T> setter)
        {
            _entries.Add(new DbMapperEntry<TC, T>(column, setter));
            return this;
        }

        public DbMapper<TC> Map<T1, T2>(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, Action<TC, T1, T2> setter)
        {
            _entries.Add(new DbMapperEntry<TC, T1, T2>(column1, column2, setter));
            return this;
        }

        public DbMapper<TC> Map<T1, T2, T3>(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, DbTypedColumn<T3> column3, Action<TC, T1, T2, T3> setter)
        {
            _entries.Add(new DbMapperEntry<TC, T1, T2, T3>(column1, column2, column3, setter));
            return this;
        }

        public DbMapper<TC> Map<T1, T2, T3, T4>(DbTypedColumn<T1> column1, DbTypedColumn<T2> column2, DbTypedColumn<T3> column3, DbTypedColumn<T4> column4, Action<TC, T1, T2, T3, T4> setter)
        {
            _entries.Add(new DbMapperEntry<TC, T1, T2, T3, T4>(column1, column2, column3, column4, setter));
            return this;
        }

        public DbMapper<TC> Map<TE>(DbMapper<TE> mapper, Action<TC, List<TE>> setter) where TE : new()
        {
            _entries.Add(new GroupMapper<TC, TE>(mapper, setter));
            return this;
        }

        public void AddEntries(DbMapper<TC> mapper)
        {
            _entries.AddRange(mapper._entries);
        }

        public DbMapper<TC> Group<TE>(DbMapper<TE> mapper, Action<TC, List<TE>> setter) where TE : new()
        {
            _groupMapper = new GroupMapper<TC, TE>(mapper, setter);
            return this;
        }

        public DbMapper<TC> GroupWhen<T>(DbTypedColumn<T> column, Func<T, T, bool> predicate)
        {
            _explicitGroupPredicate = new GroupPredicate<T>(column, predicate);
            return this;
        }

        public DbMapper<TC> GroupWhen(Func<TC, TC, bool> predicate)
        {
            _groupPredicate = predicate;
            return this;
        }

        public void BindColumns(ISqlBuilder sqlBuilder)
        {
            foreach (var entry in _entries)
            {
                entry.BindColumns(sqlBuilder);
            }

            _groupMapper?.BindColumns(sqlBuilder);
            _explicitGroupPredicate?.BindColumns(sqlBuilder);
        }

        public bool IsNull => _entries.All(e => e.IsNull);

        public void NewList()
        {
            EntityList = new List<TC>();
        }

        public void AddEntity()
        {
            if (_groupMapper != null)
            {
                if (_groupPredicate != null)
                {
                    var entity = NewEntity();

                    if (_groupPredicate(EntityList.LastOrDefault(), entity))
                    {
                        if (EntityList.Any())
                        {
                            _groupMapper.SetList(EntityList.Last());
                        }

                        EntityList.Add(entity);

                        _groupMapper.NewList();
                    }
                }
                else if (_explicitGroupPredicate == null || _explicitGroupPredicate.GroupChanged() || !EntityList.Any())
                {
                    if (EntityList.Any())
                    {
                        _groupMapper.SetList(EntityList.Last());
                    }

                    var entity = NewEntity();

                    EntityList.Add(entity);

                    _groupMapper.NewList();
                }

                _groupMapper.AddEntity();
            }
            else if (_explicitGroupPredicate == null || _explicitGroupPredicate.GroupChanged() || !EntityList.Any())
            {
                EntityList.Add(NewEntity());
            }
            else
            {
                var entity = EntityList.Last();

                foreach (var entry in _entries)
                {
                    entry.SetValues(entity);
                }
            }
        }

        public void AddInstanceInitializeAction(Action<TC> precedingAction)
        {
            _instanceInitializingActions.Add(precedingAction);
        }

        public void AddInstanceFinalizeAction(Action<TC> succeedingAction)
        {
            _instanceFinalizingActions.Add(succeedingAction);
        }

        public void End()
        {
            if (_groupMapper != null && EntityList.Any())
            {
                _groupMapper.SetList(EntityList.Last());
            }
        }

        private TC NewEntity()
        {
            var entity = _newEntity == null ? new TC() : _newEntity();

            foreach (var initializeAction in _instanceInitializingActions)
            {
                initializeAction(entity);
            }

            foreach (var entry in _entries)
            {
                entry.SetValues(entity);
            }

            foreach (var finalizeAction in _instanceFinalizingActions)
            {
                finalizeAction(entity);
            }

            return entity;
        }
    }
}
