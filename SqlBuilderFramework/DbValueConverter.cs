﻿using System;

namespace SqlBuilderFramework
{
    public class DbValueConverter
    {
        public static object ChangeType(object value, Type t)
        {
            if (value == null)
            {
                if (t.IsValueType)
                {
                    return Activator.CreateInstance(t);
                }

                return null;
            }

            var valueType = t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>) ? t.GetGenericArguments()[0] : t;

            if (valueType == typeof(bool))
            {
                return DbResultSet.ToBool(value);
            }

            if (valueType == typeof(Guid))
            {
                if (value is Guid)
                {
                    return value;
                }

                return Guid.Parse(value.ToString());
            }

            if (valueType.IsEnum)
            {
                if (value is string s)
                {
                    return Enum.Parse(valueType, s);
                }

                return Enum.ToObject(valueType, value);
            }

            return Convert.ChangeType(value, valueType);
        }
    }
}
