﻿using System;
using System.Data;
using System.Linq;
using System.Text;

namespace SqlBuilderFramework
{
    public class SqlBuilderUpdate<T> : SqlBuilderBase<T> where T : DbTable
    {
        private readonly Assignments _assignments = new Assignments();

        public override StatementType StatementType => StatementType.Update;

        public DbConstraint Constraint { get; private set; }

        public SqlBuilderUpdate(T table, IDatabase database = null)
            : base(table, database)
        {
        }

        public SqlBuilderUpdate<T> Set(params DbAssignment[] assignments)
        {
            _assignments.Add(assignments);

            return this;
        }

        public SqlBuilderUpdate<T> SetEntity<TE>(TE newEntity, TE currentEntity) where TE : class
        {
            _assignments.AddAssignmentsFromEntity(Table, newEntity, currentEntity);

            return this;
        }

        public SqlBuilderUpdate<T> Where(DbConstraint constraint)
        {
            Constraint = constraint;
            return this;
        }

        public SqlBuilderUpdate<T> And(DbConstraint constraint)
        {
            if (Constraint == null)
                return Where(constraint);

            Constraint &= constraint;
            return this;
        }

        public SqlBuilderUpdate<T> Or(DbConstraint constraint)
        {
            if (Constraint == null)
                return Where(constraint);

            Constraint |= constraint;
            return this;
        }

        public SqlBuilderUpdate<T> Map<TC>(DbTypedColumn<TC> column, Action<TC> action)
        {
            Bind(column).Setter = action;
            return this;
        }

        public override ISqlBuilderReader ExecuteReader()
        {
            if (Database.Provider == DatabaseProvider.Oracle)
            {
                return ExecuteOracleNonQueryReader();
            }

            return base.ExecuteReader();
        }

        public override string CreateSql(BuilderContext builderContext, bool usePlaceholder)
        {
            var sql = new StringBuilder();

            sql.Append("UPDATE ");
            sql.Append(Table.Definition(builderContext, usePlaceholder));
            sql.Append(" SET ");
            sql.Append(string.Join(", ", _assignments.AssignmentColumns.Select(columnAssignments => columnAssignments.Column.Sql(builderContext, false) + " = " + columnAssignments.AssignedColumns[0].Sql(builderContext, usePlaceholder))));
            if (Constraint != null)
            {
                sql.Append(" WHERE ");
                sql.Append(Constraint.Sql(builderContext, usePlaceholder));
            }

            return sql.ToString();
        }
    }
}
