﻿using System;
using System.Data;
using System.Linq;
using System.Text;

namespace SqlBuilderFramework
{
    public enum JoinMode
    {
        Inner,
        Left,
        Right,
        Outer
    }

    public abstract class DbTable
    {
        public abstract string Definition(BuilderContext builderContext, bool usePlaceholder);

        public abstract DbTable Find(ISqlBuilder query);

        public DbJoinedTable InnerJoin(DbTable rightTable)
        {
            return new DbJoinedTable(this, rightTable, JoinMode.Inner);
        }

        public DbJoinedTable InnerJoin(DbTable rightTable, DbConstraint constraint)
        {
            return new DbJoinedTable(this, rightTable, JoinMode.Inner, constraint);
        }

        public DbJoinedTable LeftJoin(DbTable rightTable)
        {
            return new DbJoinedTable(this, rightTable, JoinMode.Left);
        }

        public DbJoinedTable LeftJoin(DbTable rightTable, DbConstraint constraint)
        {
            return new DbJoinedTable(this, rightTable, JoinMode.Left, constraint);
        }

        public virtual DbConstraint ForeignKeyConstraintFor(DbTable dbTable)
        {
            return null;
        }

        public virtual T Get<T>() where T : class
        {
            return this as T;
        }

        public virtual object Get(Type sourceType)
        {
            return sourceType.IsAssignableFrom(GetType()) ? this : null;
        }
    }

    public class DbPlainTable : DbTable
    {
        private readonly string _name;
        private readonly string _schemaName;

        public DbPlainTable(string name, string schemaName = null)
        {
            _name = name;
            _schemaName = schemaName;
        }

        public override string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var sqlName = $"{builderContext.IdentifierBeginEscapeCharacter}{_name}{builderContext.IdentifierEndEscapeCharacter}";

            if (builderContext.SchemaSupported && !string.IsNullOrEmpty(_schemaName))
            {
                sqlName = $"{builderContext.IdentifierBeginEscapeCharacter}{_schemaName}{builderContext.IdentifierEndEscapeCharacter}.{sqlName}";
            }

            if (builderContext.StatementType == StatementType.Select)
            {
                sqlName += " AS " + builderContext.DefineAlias(this, builderContext);
            }

            return sqlName;
        }

        public override DbTable Find(ISqlBuilder query)
        {
            return null;
        }
    }

    public class DbJoinedTable : DbTable
    {
        private readonly DbTable _leftTable;
        private readonly DbTable _rightTable;
        private readonly JoinMode _joinMode;
        private DbConstraint _constraint;

        public DbJoinedTable(DbTable leftTable, DbTable rightTable, JoinMode joinMode)
        {
            _leftTable = leftTable;
            _rightTable = rightTable;
            _joinMode = joinMode;
        }

        public DbJoinedTable(DbTable leftTable, DbTable rightTable, JoinMode joinMode, DbConstraint constraint)
        {
            _leftTable = leftTable;
            _rightTable = rightTable;
            _joinMode = joinMode;
            _constraint = constraint;
        }

        public DbJoinedTable On(DbConstraint constraint)
        {
            _constraint = constraint;
            return this;
        }

        public override string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = new StringBuilder();

            // Left table
            result.Append(_leftTable.Definition(builderContext, usePlaceholder));

            // Join mode
            result.Append(" ");
            switch (_joinMode)
            {
                case JoinMode.Inner:
                    result.Append("INNER JOIN");
                    break;
                case JoinMode.Left:
                    result.Append("LEFT OUTER JOIN");
                    break;
                case JoinMode.Right:
                    if (builderContext.DatabaseProvider == DatabaseProvider.Sqlite)
                    {
                        throw new NotImplementedException("Right outer join not supported");
                    }
                    else
                    {
                        result.Append("RIGHT OUTER JOIN");
                    }
                    break;
                case JoinMode.Outer:
                    if (builderContext.DatabaseProvider == DatabaseProvider.Sqlite)
                    {
                        throw new NotImplementedException("Full outer join not supported");
                    }
                    else
                    {
                        result.Append("FULL OUTER JOIN");
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // Right table
            result.Append(" ");
            result.Append(_rightTable.Definition(builderContext, usePlaceholder));

            // On clause
            var constraint = _constraint ?? _leftTable.ForeignKeyConstraintFor(_rightTable) ?? _rightTable.ForeignKeyConstraintFor(_leftTable);
            if (constraint != null)
            {
                result.Append(" ON ");
                result.Append(constraint.Sql(builderContext, usePlaceholder));
            }

            return result.ToString();
        }

        public override DbTable Find(ISqlBuilder query)
        {
            DbTable table = null;

            if (_leftTable != null)
                table = _leftTable.Find(query);
            if (table == null && _rightTable != null)
                table = _rightTable.Find(query);
            return table;
        }

        public override DbConstraint ForeignKeyConstraintFor(DbTable dbTable)
        {
            return _leftTable.ForeignKeyConstraintFor(dbTable) ?? dbTable.ForeignKeyConstraintFor(_leftTable) ?? _rightTable.ForeignKeyConstraintFor(dbTable) ?? dbTable.ForeignKeyConstraintFor(_rightTable);
        }

        public override T Get<T>()
        {
            return _leftTable.Get<T>() ?? _rightTable.Get<T>() ?? base.Get<T>();
        }

        public override object Get(Type sourceType)
        {
            return _leftTable.Get(sourceType) ?? _rightTable.Get(sourceType) ?? base.Get(sourceType);
        }
    }

    public class DbInlineTable : DbTable
    {
        private readonly ISqlBuilder _query;

        public DbInlineTable(ISqlBuilder query)
        {
            _query = query;
        }

        public override string Definition(BuilderContext builderContext, bool usePlaceholder)
        {
            var result = "(" + _query.Sql(builderContext, usePlaceholder, out var subContext) + ")";

            if (builderContext.StatementType == StatementType.Select)
            {
                result += " AS " + builderContext.DefineAlias(this, subContext);
            }

            return result;
        }

        public override DbTable Find(ISqlBuilder query)
        {
            return _query == query ? this : null;
        }
    }

    public class DbInlineTable<T> : DbInlineTable
    {
        public T Columns { get; }

        public DbInlineTable(ISqlBuilder query, DbColumn[] dbColumns, Type type)
            : base(query)
        {
            Columns = (T)Activator.CreateInstance(type, dbColumns.Select(c => (object)c.ToInlineTableColumn(this)).ToArray());
        }
    }
}
