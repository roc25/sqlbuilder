using System;

namespace SqlBuilderFramework
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class DbColumnAttribute : Attribute
    {
        public readonly string ColumnProperty;
        public readonly Type TableType;
        public readonly bool ReadOnly;

        public DbColumnAttribute(string columnProperty = null, bool readOnly = false)
        {
            ColumnProperty = columnProperty;
            ReadOnly = readOnly;
        }

        public DbColumnAttribute(Type tableType, string columnProperty = null, bool readOnly = false)
        {
            TableType = tableType;
            ColumnProperty = columnProperty;
            ReadOnly = readOnly;
        }

        public DbColumn GetColumnFromAnnotation(DbTable table, string memberName)
        {
            var columnName = ColumnProperty ?? memberName;
            var tableSource = TableType != null ? table.Get(TableType) : table;
            var columnsFieldInfo = tableSource.GetType().GetField("Columns");

            if (tableSource != null && columnsFieldInfo != null)
            {
                var columns = columnsFieldInfo.GetValue(tableSource);

                var columnProperty = columns.GetType().GetProperty(columnName);

                if (columnProperty != null)
                {
                    return (DbColumn)columnProperty.GetValue(columns);
                }
            }

            return null;
        }
    }
}