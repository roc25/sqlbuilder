﻿namespace SqlBuilderFramework
{
    public static class SqlBuilder
    {
        public static SqlBuilderQuery<T> SelectFrom<T>(T dataSource) where T : DbTable
        {
            return new SqlBuilderQuery<T>(dataSource);
        }

        public static SqlBuilderInsert<T> InsertInto<T>(T table) where T : DbTable
        {
            return new SqlBuilderInsert<T>(table);
        }

        public static SqlBuilderUpdate<T> UpdateTable<T>(T table) where T : DbTable
        {
            return new SqlBuilderUpdate<T>(table);
        }

        public static SqlBuilderDelete<T> DeleteFrom<T>(T table) where T : DbTable
        {
            return new SqlBuilderDelete<T>(table);
        }

        public static QueryCommandBridge Select => new QueryCommandBridge();

        public static InsertCommandBridge Insert => new InsertCommandBridge();

        public static UpdateCommandBridge Update => new UpdateCommandBridge();

        public static DeleteCommandBridge Delete => new DeleteCommandBridge();

        public class QueryCommandBridge
        {
            private readonly IDatabase _database;

            public QueryCommandBridge(IDatabase database = null)
            {
                _database = database;
            }

            public SqlBuilderQuery<T> From<T>(T table) where T : DbTable
            {
                return new SqlBuilderQuery<T>(table, _database);
            }
        }

        public class InsertCommandBridge
        {
            private readonly IDatabase _database;

            public InsertCommandBridge(IDatabase database = null)
            {
                _database = database;
            }

            public SqlBuilderInsert<T> In<T>(T table) where T : DbTable
            {
                return new SqlBuilderInsert<T>(table, _database);
            }
        }

        public class UpdateCommandBridge
        {
            private readonly IDatabase _database;

            public UpdateCommandBridge(IDatabase database = null)
            {
                _database = database;
            }

            public SqlBuilderUpdate<T> In<T>(T table) where T : DbTable
            {
                return new SqlBuilderUpdate<T>(table, _database);
            }
        }

        public class DeleteCommandBridge
        {
            private readonly IDatabase _database;

            public DeleteCommandBridge(IDatabase database = null)
            {
                _database = database;
            }

            public SqlBuilderDelete<T> From<T>(T table) where T : DbTable
            {
                return new SqlBuilderDelete<T>(table, _database);
            }
        }
    }
}
