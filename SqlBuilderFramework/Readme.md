SQL Builder Framework
=====================

This framework is for developers who are familiar with SQL but want a type safe environment to create database commands.

Therefore this is a **database first** approach and not a code first approach. It doesn't put the database into a straitjacket.
Instead the database schema is converted into a properly typed C# code environment thus preserving all database capabilities and still have the C# compilier do the work for you.

SqlBuilder does not attempt to guess how database columns are to be mapped to your business classes. It provides an easy way to explicitly map the properties of your business class to database columns (select expressions). Thus you can profit from SQL features like data conversions, expressions and inner selects.

With SqlBuilder you can build very fast database applications.

In contrast to Entity Framework, SqlBuilder removes the need to use AutoMapper. AutoMapper often leads to a lot of similar but not identical classes, making changes awkward and obfuscating data conversions.

SqlBuilderFramework does not use reflection or expression parsing techniques (linq) for writing SQL statements. This makes it easier to change the code or find bugs but has also some syntactic drawbacks. For example the operators '&&' and '||' can't be used to form 'AND' or 'OR' statements.


Features
--------

Bulk insert.

An example
---------

    // The database interface (usually a member of your controlling class retrieved from an IOC container)

    var database = new SqliteDatabase(":memory:");

    // MyData is your business logic data class.
    // The only requirement is to have a default constructor.
    // This is not a DTO.
    // The DTOs can be auto-generated from an SQL script with the TableGenerator.

    class MyData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Timestamp { get; set; }
        public int? Number { get; set; }
    }

    // Create a mapper that defines how to map the values from the database into MyData

    var mapper = new DbMapper<MyData>()
        .map(Tables.MyDbTable.ColId, (p, x) => p.Id = x)
        .map(Tables.MyDbTable.ColName, (p, x) => p.Name = x)
        .map(Tables.MyDbTable.ColTimestamp, (p, x) => p.Timestamp = x)
        .map(Tables.MyDbTable.ColNumber, (p, x) => p.Number = x);

    // Create the query

    var query = database.Select
        .From(Tables.MyDbTable)
        .Where(Tables.MyDbTable.ColTimestamp < DateTime(2020, 1, 1));

    // Execute the query

    IEnumerable<MyData> result = query.ReadAll(mapper);
